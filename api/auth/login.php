<?php
require_once '../core/config.php';

if(isset($_SESSION['system']['userid_'])){
  header("Location: ../index.php");
  exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$GLOBALS['config']['company']['name'];?></title>
    <!-- Favicon -->
    <link rel="icon" href="../../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../../assets/css/argon.css?v=1.2.0" type="text/css">

    <script src="../../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="../../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/js/jquery.easing.min.js"></script>
    <script src="../../assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>

    <style>
        /* linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(23, 22, 22)), */
        .loginBg{
        background-image: url('../../assets/img/login.jpeg');
        background-repeat: no-repeat;
        object-fit: cover;
        background-size: cover;
        }
        body{
        font-size: 14px;
        width: 100%;
        }

        .modal-black{
            background: #000000de;
        }
    </style>
    <script>
        function alertMe(message, type) {
            $.notify({
                // options
                message: message 
            },{
                // settings
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                    from: "bottom",
                    align: "right"
                },
                z_index: 1055,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });
        }
    </script>
</head>
<body class="loginBg">
<div class="container">
    <div class="row justify-content-end">
        <div class="col-lg-5 col-md-7" style="margin-top: 9%;">
            <div class="card bg-secondary border-0 mb-0">
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <h3>Welcome back!</h3>
                    </div>
                    <form role="form">
                    <div class="form-group mb-3">
                        <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                        </div>
                        <input class="form-control" placeholder="Username" type="text" id="auth_username" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                        </div>
                        <input class="form-control" placeholder="Password" type="password" id="auth_password">
                        </div>
                    </div>
                    <span class="text-muted">
                    <?php
                        if(isset($_SESSION['system']['error_']))
                        {
                        $message = $_SESSION['system']['error_'];
                        if(!empty($message)){
                            echo '<span id="warning" class="animated shake text-danger" class="isa_error">
                            <i class="fa fa-exclamation-triangle"></i>
                            '.$message.'
                            </span>';
                        }
                        }
                    ?>
                    </span>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary my-4" onclick="login_me()">Sign in</button>
                    </div>
                    </form>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                    <!-- <a href="#" class="text-light"><small>Forgot password?</small></a> -->
                </div>
                <div class="col-6 text-right">
                    <a href="#" class="text-light" onclick="createNewUserModal()"><small>Create new account</small></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once '../views/modals/add_new_user_modal.php';?>
</body>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <!-- Optional JS -->
    <script src="../../assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="../../assets/vendor/chart.js/dist/Chart.extension.js"></script>
    <!-- Argon JS -->
    <script src="../../assets/js/argon.js?v=1.2.0"></script>

    <script type="text/javascript">
        function login_me() {
            var username = $("#auth_username").val();
            var password = $("#auth_password").val();

            $.post("../ajax/processLogin.php",{
                username: username,
                password: password
            },function(data,status){
                if(data == 1){
                    window.location = '../index.php';
                }else{
                    location.reload();
                }
            });
        }

        $('#auth_password').on('keypress', function (e) {
            if(e.which === 13){
                login_me();
            }
        });

        function createNewUserModal() {
            $("#add-new-user-modal").modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }

        function createAccount() {
            var name = $("#opt_name").val();
            var username = $("#opt_username").val();
            var password = $("#opt_password").val();
            var email = $("#opt_email").val();

            if(name == "" || username == "" || password == "" || email == ""){
                alertMe("Missing a field", 'warning');
            }else{
                $.post("../ajax/save_new_account.php",{
                    name: name,
                    username: username,
                    password: password,
                    email: email
                },function(data){
                    if(data == 1){
                        alertMe("Account creation successful, please login", "success");
                        $("#add-new-user-modal").modal('hide');
                    }else if(data == 2){
                        alertMe("Account already exist", "warning");
                    }else{
                        alertMe('Error!', 'danger');
                    }

                    
                });
            }
        }

        $(document).ready(function() {
            $("#auth_username").focus();
	    });
  </script>

</html>