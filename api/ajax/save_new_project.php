<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$project_name = clean($_POST['project_name']);
$project_description = clean($_POST['project_description']);
$projectCode = generateRandomString(10);

$data = array(
    'projectCode' => $projectCode,
    'projectName' => $project_name,
    'projectDescription' => $project_description,
    'proj_pm' => $user_id 
);
$result = INSERT_QUERY("tbl_project", $data);
echo $result;
