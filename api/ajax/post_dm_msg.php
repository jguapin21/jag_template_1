<?php
include '../core/config.php';
$msg = clean($_POST['msg']);
$receiver_id = $_POST['receiver_id'];
$user_id = $_SESSION["system"]["userid_"];
$date_sent = date("Y-m-d H:i:s");
$convo_id = -1;

if($receiver_id != "" && $user_id != ""){
    $data = array(
        'convo_id' => $convo_id,
        'sender_id' => $user_id,
        'channel_id' => $receiver_id,
        'date_added' => $date_sent,
        'msg' => $msg
    );

    $msg_id = INSERT_QUERY("tbl_convo_msg", $data, "Y");
    if($msg_id){
        insertToNotif($convo_id, $msg_id, $receiver_id, $user_id, $date_sent);
    }
}else{
    echo 3;
}

function insertToNotif($convo_id, $msg_id, $receiver_id, $sender_id, $date_sent)
{
    $data = array(
        'convo_id' => $convo_id,
        'channel_id' => 0,
        'msg_id' => $msg_id,
        'receiver_id' => $receiver_id,
        'sender_id' => $sender_id,
        'date_sent' => $date_sent
    );

    INSERT_QUERY("tbl_convo_notif", $data);
}