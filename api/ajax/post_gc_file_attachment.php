<?php
include '../core/config.php';

$file = $_POST['attachment_val'];
$filename = $_POST['filename'];
$msg = clean($_POST['msg']);
$type = $_POST['type'];
$user_id = $_SESSION["system"]["userid_"];
$date_sent = date("Y-m-d H:i:s");
if($type == "GC"){
    $ch_id = $_SESSION['chat']['channel'];
    $convo_id = $_SESSION['chat']['convo'];
}else{
    $ch_id = $_POST['recpient_id'];
    $convo_id = -1;
}
if($ch_id != "" && $convo_id != ""){
    $crop_file = $file;
    list($type, $crop_file) = explode(';', $crop_file);
    list(, $crop_file) = explode(',', $crop_file);
    $extension = explode('.',$filename);

    $crop_file = base64_decode($crop_file);
    $file_name = $convo_id.$ch_id.'-'.date('ymdhis').'.'.end($extension);
    $fileSlug = '../'.MSG_ATTACHMENT_BASEPATH.$file_name; //, $crop_file
    if(file_put_contents($fileSlug, $crop_file)) {

        $data = array(
            'convo_id' => $convo_id,
            'sender_id' => $user_id,
            'channel_id' => $ch_id,
            'date_added' => $date_sent,
            'msg' => $msg,
            'slug' => $file_name,
            'filename' => $filename
        );
    
        $msg_id = INSERT_QUERY("tbl_convo_msg", $data, "Y");
        if($msg_id){
            insertToGCNotif($convo_id, $msg_id, $ch_id, $user_id, $date_sent, $type);
            echo 1;
        }
    }else{
        echo 0;
    }
}else{
    echo 3;
}

function insertToGCNotif($convo_id, $msg_id, $channel_id, $sender_id, $date_sent, $type)
{
    if($type == "GC"){
        $loop_convo_users = SELECT_LOOP_QUERY("*","tbl_convo_member","convo_id = '$convo_id' AND member_id != '$sender_id'");
        if(count($loop_convo_users) > 0){
            foreach($loop_convo_users as $userList){
                $data = array(
                    'convo_id' => $convo_id,
                    'channel_id' => $channel_id,
                    'msg_id' => $msg_id,
                    'receiver_id' => $userList[member_id],
                    'sender_id' => $sender_id,
                    'date_sent' => $date_sent
                );

                INSERT_QUERY("tbl_convo_notif", $data);
            }
        }
    }else{
        $data = array(
            'convo_id' => $convo_id,
            'channel_id' => 0,
            'msg_id' => $msg_id,
            'receiver_id' => $channel_id,
            'sender_id' => $sender_id,
            'date_sent' => $date_sent
        );

        INSERT_QUERY("tbl_convo_notif", $data);
    }
}