<?php
include '../core/config.php';
?>
<div class="card" style="margin-top: 10px;">
    <div class="card-body" id="crd_bdy">
        <textarea class="card-title note-title" rows="1" placeholder="Title" maxlength="999" dir="ltr" style="height: 20px;font-size: 14px; font-style: bolder; font-family: inherit;margin-bottom: 0px;" id="note_title"></textarea>
        
        <div class="card-text note-content mb-2" rows="1" contenteditable="true" placeholder="Take a note…" maxlength="19999" dir="ltr" style="font-size: 12px; font-family: inherit;white-space: pre-wrap;-webkit-user-modify: read-write-plaintext-only;" id="note_content"></div>
        
        <div id="note_btns">
            <a href="#" class="btn btn-success btn-sm" onclick="saveNote()">Done</a>
            <a href="#" class="btn btn-link btn-sm" onclick="cancelNote()">Cancel</a>
        </div>
    </div>
</div>