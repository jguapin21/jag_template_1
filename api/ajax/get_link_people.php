<?php
// include '../core/config.php';
// $chat = new Chat();
// $isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"], $_SESSION['chat']['convo']);
// $loop_lp = $chat->getPeople();
// $user_id = $_SESSION["system"]["userid_"];

// if($isChatAdmin == 1){
//     $data .= '<div style="cursor: pointer;text-align: center;background: #48536f;border-radius: 4px;padding: 3px;margin: 10px;width: -webkit-fill-available;"><span class="text-muted" onclick="addPeopleToConvo()" style="font-family: myFirstFont;font-size: 14px;color: #fff !important;">Invite a friend</span></div>';
// }

// if(count($loop_lp) < 1){
//     $data .= '<div style="text-align: center;padding: 3px;margin: 10px;width: -webkit-fill-available;">need to select or add conversation</div>';
// }else{
//     foreach($loop_lp as $lp_list){
//         $hasCrown = $chat->getConvoAdmin($lp_list[member_id], $lp_list[convo_id]);
//         $user_avatar = getUserAvatar($lp_list[member_id]);
//         $isOnline = '<i class="fas fa-circle" style="color: green;font-size: 8px;position: relative;top: 10px;right: 11px;border-radius: 50%;padding: 2px;margin: 0px;background: #ffffff;"></i>';
//         $owner = ($hasCrown == 1)?'<i class="fas fa-crown" style="color: orange;font-size: 8px;position: relative;top: 10px;right: 11px;border-radius: 50%;padding: 2px;margin: 0px;background: #ffffff;"></i> ':$isOnline;
//         $allowUpdateNickName = ($isChatAdmin == 1)?'onclick="openChatUserSettings(\''.$lp_list[member_id].'\',\''.clean($lp_list["nickname"]).'\')"':($user_id == $lp_list[member_id])?'onclick="openChatUserSettings(\''.$lp_list[member_id].'\',\''.clean($lp_list["nickname"]).'\')"':""; ;
//         $hasNickName = ($lp_list["nickname"] == "")?clean($lp_list["member_name"]):clean($lp_list["nickname"]);

//         $data .= '<div class="ch-padd-hover mb-1 mt-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;padding: 5px;border: 0px !important;width: -webkit-fill-available;" '.$allowUpdateNickName.'><div style="width: 100%;display: flex;justify-content: center;flex-direction: row;align-items: center;align-content: center;"><img src='.$user_avatar.' style="width: 25px; height: 25px;object-fit: cover;" class="avatar rounded-circle" data-toggle="tooltip" data-placement="left">'.$owner.'<h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 0px;">'.$hasNickName.'</h4></div></div>';
//     }
// }

// echo $data;

include '../core/config.php';
$chat = new Chat();
$user_id = $_SESSION["system"]["userid_"];
$isChatAdmin = $chat->getConvoAdmin($user_id, $_SESSION['chat']['convo']);
$loop_lp = $chat->getPeople();
$response = array();

if(count($loop_lp) > 0){
    foreach($loop_lp as $lp_list){
        $hasCrown = $chat->getConvoAdmin($lp_list[member_id], $lp_list[convo_id]);
        $hasNickName = ($lp_list["nickname"] == "")?clean($lp_list["member_name"]):clean($lp_list["nickname"]);
        $isAdmin = ($user_id == $lp_list[member_id])?$isChatAdmin:0;
        $canChangeNickName = ($user_id == $lp_list[member_id])?1:$isAdmin;
        $data = array(
            'people' => array(
                'id' => $lp_list[member_id],
                'avatar' => getUserAvatar($lp_list[member_id]),
                'nickName' => $hasNickName,
                'hasCrown' => $hasCrown,
                'canChangeNickName' => $canChangeNickName
            ),
            'group_id' => $lp_list[convo_id],
            'isChatAdmin' => $isAdmin
        );
        array_push($response,$data);
    }
}
echo json_encode($response);