<?php
include '../core/config.php';

$loop_n = UserNotes();
if(count($loop_n) < 1){
echo "no data available";
}else{
    foreach($loop_n as $note_list){
        $noteID = "update_note_content_".$note_list[id];
        echo '
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                    <textarea class="card-title note-title" rows="1" placeholder="Title" maxlength="999" dir="ltr" style="height: 20px;font-size: 14px; font-style: bolder; font-family: inherit;margin-bottom: 0px;" id="update_note_title_'.$note_list[id].'">'.$note_list["title"].'</textarea>

                    <div class="card-text note-content mb-2" rows="1" contenteditable="true" placeholder="Take a note…" maxlength="19999" dir="ltr" style="font-size: 12px; font-family: inherit;white-space: pre-wrap;-webkit-user-modify: read-write-plaintext-only;" id="'.$noteID.'">'.html_entity_decode($note_list["content"]).'</div>
                    
                    <div style="display: flex;flex-direction: row;justify-content: space-between;"><a href="#" class="btn btn-link btn-sm" onclick="updateNote('.$note_list[id].')">Done</a><a href="#" class="btn btn-link btn-sm" onclick="deleteNote(\''.$note_list[id].'\')" style="color: red;" title="delete note"><i class="far fa-trash-alt"></i></a>
                    </div>
                </div>
            </div>
       ';
    }
} 

?>