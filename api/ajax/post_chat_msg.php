<?php
include '../core/config.php';
// $msg_q = str_replace("<div><br></div>", " ", $_POST['msg']);
$msg = clean($_POST['msg']);
$convo_id = $_SESSION['chat']['convo'];
$ch_id = $_SESSION['chat']['channel'];
$user_id = $_SESSION["system"]["userid_"];
$date_sent = date("Y-m-d H:i:s");
if($ch_id != "" && $convo_id != ""){
    $data = array(
        'convo_id' => $convo_id,
        'sender_id' => $user_id,
        'channel_id' => $ch_id,
        'date_added' => $date_sent,
        'msg' => $msg
    );

    $msg_id = INSERT_QUERY("tbl_convo_msg", $data, "Y");
    if($msg_id){
        insertToNotif($convo_id, $msg_id, $ch_id, $user_id, $date_sent);
    }
}else{
    echo 3;
}

function insertToNotif($convo_id, $msg_id, $channel_id, $sender_id, $date_sent)
{
    $loop_convo_users = SELECT_LOOP_QUERY("*","tbl_convo_member","convo_id = '$convo_id' AND member_id != '$sender_id'");
    if(count($loop_convo_users) > 0){
        foreach($loop_convo_users as $userList){
            $data = array(
                'convo_id' => $convo_id,
                'channel_id' => $channel_id,
                'msg_id' => $msg_id,
                'receiver_id' => $userList[member_id],
                'sender_id' => $sender_id,
                'date_sent' => $date_sent
            );

            INSERT_QUERY("tbl_convo_notif", $data);
        }
    }
}