<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$loop_logs = SELECT_LOOP_QUERY("*","tbl_request_logs","status != 2 ORDER BY request_id DESC");
if(count($loop_logs) > 0){
    foreach($loop_logs as $logList){
        $appDate = ($logList[approve_date] == "0000-00-00 00:00:00")?'<span style="color: orange;">pending</span>':date("M d, Y", strtotime($logList[approve_date]));
        $appBy = ($logList["approved_by"] == 0)?'<span style="color: orange;">pending</span>':clean(getUserName($logList["approved_by"]));

        $delBtn = ($user_id == $logList[person_assigned])?'<a href="#" class="btn btn-link btn-sm" onclick="deleteRequest(\''.$logList[request_id].'\')" style="color: red;" title="delete request"><i class="far fa-trash-alt"></i></a>':'';

        $approvalBtn = ($user_id == 19)?'<a href="#" class="btn btn-success btn-sm" onclick="approveRequest('.$logList[request_id].')">Approve</a>':'';
        
        $footrBtn = ($logList["status"] == 0)?$approvalBtn.$delBtn:'';

        $data .= '<div class="col-12">
            <div class="card mb-0" style="margin-top: 10px;">
                <div class="card-body">
                    <div style="font-size: 14px;">
                        <table>
                            <tr>
                                <td class="rb_data">Created date</td>
                                <td class="rb_mid">:</td>
                                <td>'.date("M d, Y", strtotime($logList[request_date])).'</td>
                            </tr>
                            <tr>
                                <td class="rb_data">Created by</td>
                                <td class="rb_mid">:</td>
                                <td>'.clean(getUserName($logList[person_assigned])).'</td>
                            </tr>
                            <tr>
                                <td class="rb_data">Approve date</td>
                                <td class="rb_mid">:</td>
                                <td>'.$appDate.'</td>
                            </tr>
                            <tr>
                                <td class="rb_data">Approved by</td>
                                <td class="rb_mid">:</td>
                                <td>'.$appBy.'</td>
                            </tr>
                            <tr>
                                <td class="rb_data">Requested By</td>
                                <td class="rb_mid">:</td>
                                <td>'.$logList["requested_by"].'</td>
                            </tr>
                            <tr>
                                <td class="rb_data">Description</td>
                                <td class="rb_mid">:</td>
                                <td class="rb_desc">'.$logList["logs"].'</td>
                            </tr>
                            <tr>
                                <td class="rb_data">Remarks</td>
                                <td class="rb_mid">:</td>
                                <td>'.$logList["remarks"].'</td>
                            </tr>
                        </table>
                    </div>
                    
                    <div style="display: flex;flex-direction: row;justify-content: space-between;">'.$footrBtn.'</div>
                </div>
            </div>
        </div>';
    }
}


echo $data;