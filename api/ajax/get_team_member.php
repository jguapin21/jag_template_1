<?php
include '../core/config.php';
$teamCode = $_POST['teamSelected'];
$_SESSION['system']['selected_team'] = $teamCode;
$project = new Project();
$data = '<option value="">-- select member --</option>';
$loopteam = $project->getMemberInTeam($teamCode);
if(count($loopteam) > 0){
    foreach($loopteam as $tmlist){
        $data .= '<option value="'.$tmlist[user_id].'">'.getUserName($tmlist[user_id]).'</option>';
    } 
}

echo $data;