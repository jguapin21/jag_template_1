<?php
include '../core/config.php';
$project = new Project();
$project_code = $_REQUEST['project_code'];
$_ispm = isProjectManager($project_code);
$selectedMember = $_REQUEST['memberSelected'];
?>
<div class="col-md-4">
    <div class="card mb-3">
        <div class="card-header" style="padding: 10px;">
        <strong>TO DO'S</strong>
        <?php
            $todo = $project->getUserTask(0,$project_code,$selectedMember);
            if(count($todo)>0){
            echo "<span style='float:right'><span class='badge badge-secondary'>".count($todo)."</span></span>";
            }
        ?>
        </div>
        <div class="card-body drops droptarget msg_chat_scroll" id="sortable1" style="padding: 8px;">
        <?php
            if(count($todo) > 0){
                foreach ($todo as $todolist) {
                    echo "<div class='ui-state-default card mb-3 c_items' id='".$todolist[taskID]."' style='font-size: 14px;margin-bottom: 5px !important;cursor: grab;'><div class='card-header' style='border-left: 3px solid ".$todolist[priority].";padding: 8px;'><div class='col-12' style='display: flex;flex-direction: row;padding: 0px;justify-content:space-between;'><small><label style='border: 1px solid #ddd;border-radius: 5px;color: #1d81e8;padding-left: 3px; padding-right: 3px;margin-bottom: 0px;'><b><i class='far fa-calendar-check'></i> ".date('m/d/Y',strtotime($todolist[date_due]))."</b></label></small><small class='text-muted float-right' onclick='deletetask($todolist[taskID])' style='color: red !important; cursor: default;'>Delete</small></div><div class='col-12' style='padding: 0px;'><div class='col-12' style='padding: 0px;'><small>CODE: ".$todolist['task_code']."</small></div><pre style='white-space: pre-wrap;font-family: inherit;font-size: 14px;'>".$todolist[task]."</pre></div></div></div>";
                }
            }
        ?>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="card mb-3">
        <div class="card-header" style="padding: 10px;">
            <strong>IN PROGRESS</strong>
            <?php
                $ongoing = $project->getUserTask(1,$project_code,$selectedMember);
                if(count($ongoing)>0){
                    echo "<span style='float:right'><span class='badge badge-secondary'>".count($ongoing)."</span></span>";
                }
            ?>
        </div>
        <div class="card-body drops droptarget msg_chat_scroll" id="sortable2" style="padding: 8px;">
            <?php
                if(count($ongoing) > 0){
                    foreach ($ongoing as $ongoinglist) {
                        echo "<div class='ui-state-default card mb-3 c_items' id='".$ongoinglist[taskID]."' style='font-size: 14px;margin-bottom: 5px !important;cursor: grab;'><div class='card-header' style='border-left: 3px solid ".$ongoinglist[priority].";padding: 8px;'><div class='col-12' style='display: flex;flex-direction: row;padding: 0px;justify-content:space-between;'><small><label style='border: 1px solid #ddd;border-radius: 5px;color: #1d81e8;padding-left: 3px; padding-right: 3px;margin-bottom: 0px;'><b><i class='far fa-calendar-check'></i> ".date('m/d/Y',strtotime($ongoinglist[date_due]))."</b></label></small><small class='text-muted float-right' onclick='deletetask($ongoinglist[taskID])' style='color: red !important; cursor: default;'>Delete</small></div><div class='col-12' style='padding: 0px;'><div class='col-12' style='padding: 0px;'><small>CODE: ".$ongoinglist['task_code']."</small></div><pre style='white-space: pre-wrap;font-family: inherit;font-size: 14px;'>".$ongoinglist[task]."</pre></div></div></div>";
                    }
                }
            ?>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="card mb-3">
        <div class="card-header" style="padding: 10px;">
            <strong>DONE</strong>
            <?php
                $done = $project->getUserTask(2,$project_code,$selectedMember);
                if(count($done)>0){
                    echo "<span style='float:right'><span class='badge badge-secondary'>".count($done)."</span></span>";
                }
            ?>
        </div>
        <div class="card-body drops droptarget msg_chat_scroll" id="sortable3" style="padding: 8px;">
            <?php
                if(count($done) > 0){
                    foreach ($done as $donelist) {
                        echo "<div class='ui-state-default card mb-3 c_items' id='".$donelist[taskID]."' style='font-size: 14px;margin-bottom: 5px !important;'><div class='card-header' style='border-left: 3px solid ".$donelist[priority].";padding: 8px;'><div class='col-12' style='display: flex;flex-direction: row;padding: 0px;justify-content:space-between;'><small><label style='border: 1px solid #ddd;border-radius: 5px;color: #1d81e8;padding-left: 3px; padding-right: 3px;margin-bottom: 0px;'><b><i class='far fa-calendar-check'></i> ".date('m/d/Y',strtotime($donelist[date_due]))."</b></label></small></div><div class='col-12' style='padding: 0px;'><div class='col-12' style='padding: 0px;'><small>CODE: ".$donelist['task_code']."</small></div><pre style='white-space: pre-wrap;font-family: inherit;font-size: 14px;'>".$donelist[task]."</pre></div></div></div>";
                    }
                }
            ?>
        </div>
    </div>
</div>