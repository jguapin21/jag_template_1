<?php
include '../core/config.php';

$user_id = $_SESSION["system"]["userid_"];
$module = $_POST['fb_module'];
$desc = $_POST['fb_desc'];

$data = array(
    'module' => $module,
    'description' => clean($desc),
    'sender' => clean(getUserName($user_id)),
    'date_created' => date("Y-m-d H:i:s")
);

$res = INSERT_QUERY("tbl_feedback", $data);
echo $res;