<?php
// include '../core/config.php';
// $convo_id = -1;
// $sender_id = $_POST['id'];
// $user_id = $_SESSION["system"]["userid_"];
// $_SESSION['chat']['recepient_id'] = $sender_id;
// $getMsg = SELECT_LOOP_QUERY("sender_id,msg,date_added,channel_id,slug,chat_id,edit_status","tbl_convo_msg","convo_id='$convo_id' AND ((channel_id='$user_id' AND sender_id = '$sender_id')  OR (channel_id='$sender_id' AND sender_id = '$user_id')) ORDER BY chat_id ASC");
// if($sender_id == ""){
//     $data .= "<div class='pl-0 pt-0 pb-0' style='border: 0px;width: 100%;text-align: center;'><h1 class='pb-0 mb-0'>Accord</h1><p class='mb-1'>Welcome human, let's have an accord!</p></div>";
// }else{
//     // has value
    // $data .= "<div class='pl-0 pt-1 pb-0' style='width: 100%;border-bottom: 1px solid #ccc;'><img src='".getUserAvatar($sender_id)."' style='width: 120px;height: 120px;object-fit: cover;' class='rounded-circle'><h1 style='word-break: break-all;margin-bottom: 5px;'>".clean(getUserName($sender_id))."</h1><p style='word-break: break-all;line-height: 19px;'>Your history starts here with @".clean(getUserName($sender_id)).".</p></div>";

//     if(count($getMsg) > 0){
//         foreach($getMsg as $msg_list){
//             //$enddata[] = getUserName($msg_list[0]).",".$msg_list[1];
//             $user_avatar = getUserAvatar($msg_list[0]);

//             $msg_slug = ($msg_list[4] != "")?'<div class="col pb-2" style="text-align: left;padding: 0px;"><img src="'.MSG_ATTACHMENT_BASEPATH.$msg_list[4].'" style="width: 100%;height: 224px;object-fit: contain;object-position: 0 0;" onclick="previewMedia(\''.MSG_ATTACHMENT_BASEPATH.$msg_list[4].'\')"></div>':'';

//             $msg_options = ($user_id == $msg_list[0])?"<div class='show-on-msg-hover' style='position: absolute;top: 3px;right: 8px;'><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 8px;margin: 2px;background: #e6e6e6;' title='edit' onclick='edit_dm_msg(".$msg_list[5].")'><i class='fa fa-pen' style='font-size: 12px;'></i></span><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 8px;margin: 2px;background: #e6e6e6;' title='delete' onclick='delete_dm_msg(".$msg_list[5].",".$convo_id.",".$msg_list[0].")'><i class='far fa-trash-alt' style='font-size: 12px;color: red;'></i></span></div>":"";

//             $isEdited = ($msg_list[6] == 1)?' &bull; Edited':'';

//             $data .= "<div class='pl-1 pt-2 pb-0 msg_hover' style='border: 0px;width: 100%;'><div class='row'><div class='col-auto pr-0 align-items-start' style='padding-top: 3px;'><a href='#' class='avatar rounded-circle' style='width: 35px; height: 35px;'><img src='".$user_avatar."' style='width: 100%;height: 100%;object-fit: cover;' class='rounded-circle' data-toggle='tooltip' data-placement='left''></a></div><div class='col pl-3 align-items-center'><div style='text-align: start;padding: 0px;'><div class='row' style='margin: 0px;'><div class='col-12' style='display: flex;width: 100px;padding: 0px;'><div style='width: 84%;'><h3 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: 400;color: #000 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;' id='channel_name'>".clean(getUserName($msg_list[0]))."</h3></div>".$msg_options."</div><div class='col-12 pl-0' style='line-height: 7px;'><small class='text-muted' style='font-size: 71% !important;'>".date("m/d/Y h:i A", strtotime($msg_list[2])).$isEdited."</small></div></div></div><textarea hidden id='dm_msg_content_box_".$msg_list[5]."' style='font-size: 1rem;font-family: myFirstFont; word-break: break-word;white-space: pre-wrap;color: #4e4e4e;'>".$msg_list[1]."</textarea><p style='font-size: 1rem;font-family: myFirstFont;word-break: break-word;white-space: pre-wrap;color: #4e4e4e; margin-bottom: 5px;line-height: 22px;' id='dm_msg_content_box'>".$msg_list[1]."</p>".$msg_slug."</div></div></div>";
//         }
//         //$_SESSION['chat']['last_line'] = end($enddata);
//     }
// }
// echo $data;


include '../core/config.php';
$convo_id = -1;
$sender_id = $_POST['id'];
$user_id = $_SESSION["system"]["userid_"];
$_SESSION['chat']['recepient_id'] = $sender_id;
$response = array();

$getMsg = SELECT_LOOP_QUERY("*","tbl_convo_msg","convo_id='$convo_id' AND ((channel_id='$user_id' AND sender_id = '$sender_id')  OR (channel_id='$sender_id' AND sender_id = '$user_id')) ORDER BY chat_id ASC");
if($getMsg){
    foreach($getMsg as $msgList){
        $enddata[] = getUserName($msgList[sender_id]).",".$msgList["msg"];
        $author = SELECT_QUERY("*","tbl_users","user_id = '$msgList[sender_id]'");
        $hasPriv = ($user_id == $msgList[sender_id])?1:0;
        $attachment_extension = ($msgList["slug"] != "" && $msgList["filename"] != "")?explode('.', $msgList["filename"]):'';
        $hasFileAtachment = ($msgList["slug"] != "" && $msgList["filename"] != "")?1:0;

        $data = array(
            'attachments' => array(
                'filename' => ($msgList["filename"] != null)?$msgList["filename"]:"",
                'size' => "",
                'url' => ($msgList["slug"] != null)?$msgList["slug"]:"",
                'file_extension' => extension_icon(end($attachment_extension)),
                'hasFileAttachment' => $hasFileAtachment
            ),
            'author' => array(
                'avatar' => getUserAvatar($msgList[sender_id]),
                'id' => $msgList[sender_id],
                'username' => clean(getUserName($msgList[sender_id]))
            ),
            'recipient' => array(
                'avatar' => getUserAvatar($sender_id),
                'id' => $sender_id,
                'username' => clean(getUserName($sender_id))
            ),
            'group_id' => $msgList[convo_id],
            'content' => html_entity_decode($msgList["msg"]),
            'edited_status' => $msgList[edit_status],
            'id' => $msgList[chat_id],
            'timestamp' => date("m/d/Y h:i A", strtotime($msgList[date_added])),
            'hasPriv' => $hasPriv
        );

        $lastLine[] = $msgList[chat_id];
        array_push($response,$data);
    }
}
$_SESSION['chat']['last_dm_line'] = end($lastLine);
$_SESSION['chat']['start'] = end($lastLine);
echo json_encode($response);


