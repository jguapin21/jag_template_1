<?php
// include '../core/config.php';
// $convo_id = $_SESSION['chat']['convo'];
// $chat = new Chat();
// $isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"], $convo_id);
// $loop_ch = $chat->getChannels($convo_id);

// if($isChatAdmin == 1){
//     $data .= '<div style="cursor: pointer;text-align: center;background: #48536f;border-radius: 4px;padding: 3px;margin: 5px 10px 10px 10px;width: -webkit-fill-available;"><span class="text-muted" onclick="addNewChannel()" style="font-family: myFirstFont;font-size: 14px;color: #fff !important;">Add new channel</span></div>';
// }

// if(count($loop_ch) < 1){
//     $data .= '<div class="px-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;border: 0px !important;width: -webkit-fill-available;background: transparent;"><h4 class="text-muted" style="font-family: myFirstFont;font-size: 14px;text-overflow: ellipsis;white-space: pre-wrap;overflow: hidden;margin-bottom: 0px;">Add channels to organize your chat messages</h4></div>';
// }else{
//     foreach($loop_ch as $ch_list){
//         $channelID = "channel_id_".$ch_list["name"];
//         $chBadgeCounter = "ch_count_".$ch_list[id];
//         $counter_pill = $chat->channel_counter($convo_id, $ch_list[id]);
//         $badgeCounterShow = '<span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;" id="'.$chBadgeCounter.'">'.$counter_pill.'</span>';
//         $settingIcon = ($isChatAdmin == 1)?'<i class="fas fa-cog show-onhover" style="cursor: pointer;font-size: 12px;padding: 5px;" onclick="openChannelSettings(\''.$ch_list[id].'\',\''.clean($ch_list[name]).'\')"></i>':'';
//         $hid_data = "<input type='hidden' id='".$channelID."' value='".$ch_list[id]."'>";

//         $data .= '<div class="ch-padd-hover px-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;border: 0px !important;width: -webkit-fill-available;background: transparent;margin-bottom: 2px">'.$hid_data.'<div onclick="chat_session_updater(\''.$ch_list[id].'\',\'channel\')" class="channel_text_width"><h4 class="text-muted channel_display_name" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;"># '.$ch_list["name"].'</h4></div><div style="align-items: baseline;justify-content: center;display: flex;">'.$badgeCounterShow.$settingIcon.'</div></div>';
//     }
// }

// echo $data;

include '../core/config.php';
$convo_id = $_SESSION['chat']['convo'];
$chat = new Chat();
$user_id = $_SESSION["system"]["userid_"];
$isChatAdmin = $chat->getConvoAdmin($user_id, $convo_id);
$loop_ch = $chat->getChannels($convo_id);
$response = array();

if(count($loop_ch) > 0){
    foreach($loop_ch as $ch_list){

        // $channelID = "channel_id_".$ch_list["name"];
        // $chBadgeCounter = "ch_count_".$ch_list[id];
        $counter_pill = $chat->channel_counter($convo_id, $ch_list[id]);
        $data = array(
            'channel' => array(
                'id' => $ch_list[id],
                'name' => $ch_list["name"],
                'unread_counter' => $counter_pill
            ),
            'author' => array(
                'avatar' => getUserAvatar($ch_list[created_by]),
                'id' => $ch_list[created_by],
                'username' => clean(getUserName($ch_list[created_by]))
            ),
            'group_id' => $ch_list[convo_id],
            'isGroupAdmin' => $isChatAdmin
        );
        array_push($response,$data);
    }
}
echo json_encode($response);


