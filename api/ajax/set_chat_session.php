<?php
include '../core/config.php';

$id = $_POST['id'];
$type = $_POST['type'];
$chat = new Chat();
switch ($type) {
    case 'convo':
        $_SESSION['chat']['convo'] = $id;
        unset($_SESSION['chat']['channel']);
        $isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"],$id);
        echo $isChatAdmin;
        break;
    
    case 'channel':
        $_SESSION['chat']['channel'] = $id;
        $isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"],$_SESSION['chat']['convo']);
        echo $isChatAdmin;
        break;
    
    case 'dm':
        $_SESSION['chat']['convo'] = $id;
        $isChatAdmin = 0;
        echo $isChatAdmin;
        break;

    default:
        $isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"],$_SESSION['chat']['convo']);
        echo $isChatAdmin;
        break;
}