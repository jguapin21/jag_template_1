<?php
// include '../core/config.php';
// $chat = new Chat();
// $isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"], $_SESSION['chat']['convo']);
// $loop_lp = $chat->DM_CONVO();
// $user_id = $_SESSION["system"]["userid_"];


//     $data .= '<div style="cursor: pointer;text-align: center;background: #48536f;border-radius: 4px;padding: 3px;margin: 10px;width: -webkit-fill-available;" onclick="addPeopleToDm()"><span class="text-muted" style="font-family: myFirstFont;font-size: 14px;color: #fff !important;">Find a friend to DM</span></div>';


// if(count($loop_lp) < 1){
//     $data .= "message someone, go!";
// }else{
//     foreach($loop_lp as $lp_list){
//         $user_avatar = getUserAvatar($lp_list[sender_id]);
//         $isOnline = '<i class="fas fa-circle" style="color: #33d018;font-size: 9px;position: absolute;bottom: 7px;left: 29px;border-radius: 50%;padding: 0px;margin: 0px;"></i>';
//         $badge_pill = DM_COUNT_USER_BADGE($lp_list[sender_id]);

        // $data .= '<div class="ch-padd-hover mb-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;padding: 3px;border: 0px !important;width: -webkit-fill-available;" onclick="get_sender_detail(\''.$lp_list[sender_id].'\', \''.clean($lp_list["member_name"]).'\')"><div style="width: 85%;display: flex;justify-content: center;flex-direction: row;align-items: center;align-content: center;padding: 3px 5px 3px 5px;"><img src='.$user_avatar.' style="width: 25px; height: 25px;object-fit: cover;" class="avatar rounded-circle" data-toggle="tooltip" data-placement="left"><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;">'.clean($lp_list["member_name"]).'</h4></div><div style="position: relative;top: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;" id="rcp_badge_pill_'.$lp_list[sender_id].'">'.$badge_pill.'</span></div></div>';
//     }
// }

// echo $data;

include '../core/config.php';
$convo_id = -1;
$chat = new Chat();
$loop_lp = $chat->DM_CONVO();
$response = array();

if(count($loop_lp) > 0){
    foreach($loop_lp as $lp_list){
        $user_avatar = getUserAvatar($lp_list[sender_id]);
        $badge_pill = DM_COUNT_USER_BADGE($lp_list[sender_id]);
        $isChatAdmin = $chat->getConvoAdmin($lp_list[sender_id], $convo_id);
        $data = array(
            'recepient' => array(
                'id' => $lp_list[sender_id],
                'name' => $lp_list["member_name"],
                'avatar' => $user_avatar
            ),
            'badge_count' => $badge_pill,
            'group_id' => $convo_id,
            'isGroupAdmin' => $isChatAdmin
        );
        array_push($response,$data);
    }
}
echo json_encode($response);