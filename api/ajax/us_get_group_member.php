<?php
include '../core/config.php';
$userid = $_SESSION['system']['userid_'];
$team_code = $_POST['team_code'];

$loop_gm = SELECT_LOOP_QUERY("user_id","tbl_team_member","teamCode = '$team_code' ORDER BY team_member_id DESC");
foreach($loop_gm as $gmList){
    $user_avatar = getUserAvatar($gmList[user_id]);
    $data .= '<li class="list-group-item ch-padd-hover mb-1" style="display: flex;align-items: center;justify-content: space-between;padding: 5px;border: 0px !important;width: -webkit-fill-available;">
                    
        <div style="width: 100%;display: flex;justify-content: center;flex-direction: row;align-items: center;align-content: center;padding: 3px 5px 3px 5px;">
            <a href="#" class="avatar rounded-circle" style="width: 35px; height: 30px;">
                <img src='.$user_avatar.' style="width: 100%;height: 100%;object-fit: cover;" class="rounded-circle" data-toggle="tooltip" data-placement="left">
            </a>
            <h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;">'.clean(getUserName($gmList[user_id])).'</h4>
        </div>
        <div class="col-2">
            <div style="align-items: baseline;justify-content: flex-end;display: flex;"><span class="badge badge-danger"></span><a href="#" class="btn btn-link btn-sm" style="color: red;"><i class="far fa-trash-alt" onclick="deleteGroupMember('.$gmList[user_id].')"></i></a></div>
        </div>

    </li>
    
    ';
}

echo $data;