<?php
include '../core/config.php';
$userid = $_SESSION['system']['userid_'];
$crop_img = $_POST['image'];

list($type, $crop_img) = explode(';', $crop_img);
list(, $crop_img) = explode(',', $crop_img);

$crop_img = base64_decode($crop_img);
$image_name = $userid.'-'.date('ymdhis').'.jpg';
$fileSlug = '../'.USER_AVATAR_BASEPATH.$image_name; //, $crop_img

// getOlAvatar
$old_data = SELECT_QUERY("slug","tbl_users","user_id = '$userid'");
$oldAvatar = ($old_data[0] == "")?'':'../'.USER_AVATAR_BASEPATH.$old_data[0];

if(file_put_contents($fileSlug, $crop_img)) {
    
    if(!empty($old_data[0])){
        if(file_exists($oldAvatar)){
            unlink($oldAvatar);
        }
    }

    $data = array(
        'slug' => $image_name
    );
    $res = UPDATE_QUERY("tbl_users",$data,"user_id = '$userid'");
    if($res){
        echo 'Uploaded!';
    }
}else{
    echo 'Error uploading image!';
}