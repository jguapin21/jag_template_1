<?php
include '../core/config.php';
$convo_id = $_SESSION['chat']['convo'];
$channel_id = $_REQUEST['channel_id'];//$_SESSION['chat']['channel'];
$user_id = $_SESSION["system"]["userid_"];
$response = array();

$channelInfo = SELECT_QUERY("*","tbl_convo_channel","convo_id = '$convo_id' AND channel_id = '$channel_id'");
$getMsg = SELECT_LOOP_QUERY("*","tbl_convo_msg","convo_id='$convo_id' AND channel_id='$channel_id' ORDER BY chat_id ASC");
if($getMsg){
    foreach($getMsg as $msgList){
        $author = SELECT_QUERY("*","tbl_users","user_id = '$msgList[sender_id]'");
        $memberData = SELECT_QUERY("nickname","tbl_convo_member","convo_id = '$convo_id' AND member_id = '$msgList[sender_id]'");
        $hasNickName = ($memberData[0] == "")?clean(getUserName($msgList[sender_id])):clean($memberData[0]);
        $hasPriv = ($user_id == $msgList[sender_id])?1:0;

        $hasFileAtachment = ($msgList["slug"] != "" && $msgList["filename"] != "")?1:0;

        $attachment_extension = ($hasFileAtachment == 1)?extension_icon(end(explode('.', $msgList["filename"]))):'';

        $data = array(
            'attachments' => array(
                'filename' => $msgList["filename"],
                'size' => "",
                'url' => $msgList["slug"],
                'file_extension' => $attachment_extension,
                'hasFileAttachment' => $hasFileAtachment
            ),
            'author' => array(
                'avatar' => getUserAvatar($msgList[sender_id]),
                'id' => $msgList[sender_id],
                'username' => $hasNickName,
                'realName' => getUserName($msgList[sender_id])
            ),
            'channel' => array(
                'id' => $msgList[channel_id],
                'name' => clean($channelInfo["name"])
            ),
            'group_id' => $msgList[convo_id],
            'content' => html_entity_decode($msgList["msg"]),
            'edited_status' => $msgList[edit_status],
            'id' => $msgList[chat_id],
            'timestamp' => date("m/d/Y h:i A", strtotime($msgList[date_added])),
            'hasPriv' => $hasPriv
        );
        
        $lastLine[] = $msgList[chat_id];
        array_push($response,$data);
    }
}
$_SESSION['chat']['last_line'] = end($lastLine);
echo json_encode($response);