<?php
include 'core/config.php';
// check session
checkLoginStatus();

// ROUTES
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$chat = new Chat();
$isChatAdmin = $chat->getConvoAdmin($_SESSION["system"]["userid_"], $_SESSION['chat']['convo']);
$project = new Project();
$last_chat_id = $_SESSION['chat']['last_line'];
$_logID = $_SESSION["system"]["userid_"];
$defaultuserAvatar = USER_AVATAR_BASEPATH."user_default_avatar.png";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Task management for your daily load.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <title><?=$GLOBALS['config']['company']['name'];?></title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"> -->
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/croppie.min.css">
    <link rel="stylesheet" href="../assets/css/argon.css" type="text/css">
    <style>
        body {
            font-family: 'myFirstFont' !important;
            background-color: #e4e4e4  !important;
        }

        .g-sidenav-pinned .sidenav {
            max-width: 64px !important;
        }

        .sidenav {
            width: 64px !important;
        }

        @media (max-width: 1199.98px)
        {
            .sidenav {
                transform: none !important;
            }
        }

        .navbar-vertical.navbar-expand-xs.fixed-left + .main-content {
            margin-left: 64px !important;
        }

        .navbar-vertical.navbar-expand-xs {
            max-width: 64px !important;
        }

        .menu_divider {
            margin-top: 5px;
            margin-bottom: 3px;
        }

        .note-content {
            background-color: transparent;
            border: none;
            padding: 2px;
            outline: none;
            overflow: hidden;
            resize: none;
            vertical-align: top;
            width: 100%;
            word-break: break-all;
            font-size: 13px !important;
        }

        .note-content:hover {
            overflow: auto;
        }

        .note-title {
            background-color: transparent;
            border: none;
            padding: 0;
            outline: none;
            overflow: hidden;
            resize: none;
            vertical-align: top;
            width: 100%;
        }

        .bg-primary {
            background-color: #343a40 !important;
        }

        /*
        *  STYLE 4
        */

        .style-4::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        .style-4::-webkit-scrollbar
        {
            width: 5px;
            background-color: #F5F5F5;
        }

        .style-4::-webkit-scrollbar-thumb
        {
            background-color: #676767;
            border: 0px solid #555555;
        }

        /* 
        *   CHAT SCROLL
        */
        .msg_chat_scroll {
            overflow: hidden;
        }
        
        .msg_chat_scroll::-webkit-scrollbar-track
        {
            /* inset 0 0 6px rgba(0,0,0,0.3) */
            -webkit-box-shadow: transparent;
            background-color: transparent;
        }

        .msg_chat_scroll::-webkit-scrollbar
        {
            width: 5px;
            background-color: transparent;
        }

        .msg_chat_scroll::-webkit-scrollbar-thumb
        {
            background-color: #676767;
            border: 0px solid #555555;
            border-radius: 4px;
        }

        .msg_chat_scroll:hover {
            overflow: auto;
        }

         /* 
        *   CHAT SCROLL STEADY
        */
        .msg_chat_scroll_steady {
            overflow: auto;
        }
        
        .msg_chat_scroll_steady::-webkit-scrollbar-track
        {
            /* inset 0 0 6px rgba(0,0,0,0.3) */
            -webkit-box-shadow: transparent;
            background-color: transparent;
        }

        .msg_chat_scroll_steady::-webkit-scrollbar
        {
            width: 5px;
            background-color: transparent;
        }

        .msg_chat_scroll_steady::-webkit-scrollbar-thumb
        {
            background-color: #676767;
            border: 0px solid #555555;
            border-radius: 4px;
        }

        .msg_chat_scroll_steady:hover {
            overflow: auto;
        }

        /* .badge{
            font-size: 10px !important;
            color: #fff !important;
            background: red !important;
            font-family: inherit !important;
        }

        .badge-circle.badge-md {
            width: 19px;
            height: 20px;
        } */

        .badge-danger {
            font-size: 9px !important;
            color: #fff !important;
            background: red !important;
            font-family: inherit !important;
        }

        .bs-tooltip-left{
            position: fixed !important;
        }

        .modal-black{
            background: #000000de;
            padding-right: 0px !important;
            overflow-y: hidden !important;
        }

        .badge-pill {
            padding-bottom: 4px !important;
        }
        
        .focused .input-group {
            box-shadow: none;
        }

        .card {
            border: 1px solid #dadada !important;
        }

        .nav-tabs-code .nav-link.active, .nav-tabs-code .nav-link:active {
            color: #ca8810;
        }

        .list-group-item {
            border: 0px !important;
        }
    </style>
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/js/jquery-ui.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/js/jquery.easing.min.js"></script>
    <script src="../assets/js/croppie.min.js"></script>
    <script src="../assets/js/paste.js"></script>
    <script src="../assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script>
        function logout()
        {
            var confirm_logout = confirm("You are about to logout.");
            if(confirm_logout == true){
                window.location = "auth/logout.php"
            }
        }

        function alertMe(message, type) {
            $.notify({
                // options
                message: message 
            },{
                // settings
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                    from: "bottom",
                    align: "right"
                },
                z_index: 1055,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });
        }
    </script>
</head>
<body class="msg_chat_scroll_steady" style="padding-right: 0px !important;">
    <audio id="msg_notif_sound">
        <source src="../assets/media/msg_sound.mp3" type="audio/mpeg">
    </audio>
    <?php include_once 'sidebar/index.php'; ?>
    <div class="main-content" id="panel">
        <?php require_once('routes/routes.php'); ?>
    </div>
    
    <div style="position: absolute;top: 8px;right: 46px;">
        <a href="http://pms.wdysolutions.com" class="btn btn-warning btn-sm">Switch back to classic</a>
    </div>
    
    <?php include_once 'views/modals/add_note_modal.php';?>
    <?php include_once 'views/modals/add_project_modal.php';?>
    <?php include_once 'views/modals/modal_chat.php';?>
    <?php include_once 'views/modals/change_log.php';?>
    <?php include_once 'views/modals/modal_request_book.php';?>
    <?php include_once 'views/modals/feedback_modal.php';?>
    <?php include_once 'views/modals/preview_media.php';?>
    <?php include_once 'views/modals/avatar_cropper.php';?>
</body>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <!-- Optional JS -->
    <script src="../assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="../assets/vendor/chart.js/dist/Chart.extension.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>

        $(document).ready(function() {
            loadUserProjects();
            change_log_checker();
        });

        function loadUserProjects() {
            $.post("ajax/get_user_projects.php",{
            },function(data){
                var user_proj_list;
                var userProjCloth = '';
                var user_projects_data = JSON.parse(data);
                for (var i = 0; i < user_projects_data.length; ++i) {
                    user_proj_list = user_projects_data[i];
                    var avName = user_proj_list.projectAvName;
                    var picTest = (1 != 1)?'<img src="../assets/user_avatar/2-200811084235.jpg" style="width: 35px;height: 35px;object-fit: cover;" class="avatar rounded-circle" title="'+user_proj_list.projectName+'">':'<div style="width: 35px;height: 35px;object-fit: cover;cursor: pointer;" class="avatar rounded-circle" title="'+user_proj_list.projectName+'">'+avName.substr(0, 2)+'</div>';
                    userProjCloth += '<li class="nav-item" onclick=\'window.location="index.php?page=myproject&q='+user_proj_list.projectCode+'"\' style="text-align: center;padding: 5px;">'+picTest+'</li>';
                }
                $("#active_project_container").html(userProjCloth);
            });
        }
    
        function openModalMyNotes(){
            getUserNote();
            $("#modal-note").modal({
                show: true
            });
        }

        function openModalNewProject(){
            $("#modal-add-project").modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }

        function change_log_show_to_user(){
            $("#modal-change-log").modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }

        function change_log_checker() {
            $.post("ajax/change_log_checker.php",{
            },function(data){
                if(data == 1){
                    change_log_show_to_user();
                }
            });
        }

        function readthechangelog() {
            $.post("ajax/change_log_read.php",{
            },function(data){
                $("#modal-change-log").modal('hide');
            });
        }

        function openModalChat(){
            refreshChatModal();
            $("#modal-chat").modal({
                show: true
            });
            $('#send_msg_content').focus();
            setTimeout(scrolling, 200);
            setTimeout(dm_msg_scrolling, 200);
        }

        // $(function () {
        //     $('[data-toggle="tooltip"]').tooltip()
        // });
        if ($('[role="dialog"]').hasClass('in') === true){
            $('body').style({ 'padding-right': "0px !important" });
        }
        function scrolling(){
            var objDiv = document.getElementById("msg_contents");
            objDiv.scrollTop = objDiv.scrollHeight;
        }

        function dm_msg_scrolling(){
            var objDiv = document.getElementById("dm_msg_contents");
            objDiv.scrollTop = objDiv.scrollHeight;
        }

        function msg_sound_notif() {
            var sndFile = document.getElementById("msg_notif_sound");
            sndFile.play();
        }

        function saveNewProject(){
            var project_name = $("#project_name_modal").val();
            var project_description = $("#project_description_modal").val();
            if(project_name == '' || project_description == ''){
                alertMe("Input all fields", "warning");
            }else{
                $.post("ajax/save_new_project.php",{
                    project_name: project_name,
                    project_description: project_description
                },function(data){
                    if(data == 1){
                        $("#modal-add-project").modal('hide');
                        location.reload();
                    }else{
                        alertMe("Error saving project", "danger");
                    }
                });
            }
        }

        function openChangeLog() {
            $("#modal-change-log").modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }

        function openModalRequestBook() {
            getRequestBookData();
            $("#modal-request-book").modal({
                show: true
            });
        }

        function openFeedBack(){
            $("#modal-add-feedback").modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }

        function previewMedia(media) {
            $("#preview_media_container").attr('src',media);
            $("#modal-preview-media").modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }

        // DM MSG BADGE COUNTER SSE
        var last_dm_count_msg;
        var dm_notif_counter = new EventSource("ajax/get_dm_notif_counter_sse.php");
        dm_notif_counter.onmessage = function(event_dm) {
            //if(last_dm_count_msg){
                // console.log(event_dm.data);
                if(event_dm.data != last_dm_count_msg && event_dm.data != ""){
                    var dm_convo = JSON.parse(event_dm.data);
                    for(dm_i in dm_convo){
                        var ol_dm_value = Number($("#dm_badge_pill").html());
                        var dm_gc_total = $("#total_msg_badge").html();
                        var totalDMGC = (Number(dm_gc_total) - ol_dm_value) + dm_convo[dm_i].count_dm_convo;
                        if(totalDMGC > 0){
                            $("#total_msg_badge").html(totalDMGC);
                            if(dm_convo[dm_i].count_dm_convo > 0){
                                msg_sound_notif(); 
                            }
                        }
                        
                        $("#dm_badge_pill").html(dm_convo[dm_i].count_dm_convo);
                        var rcp_loop = dm_convo[dm_i].rcp;
                        for (rzp in rcp_loop) {
                            // recepient count
                            $("#rcp_badge_pill_"+rzp).html(rcp_loop[rzp]);
                        }
                    }
                }
                last_dm_count_msg = event_dm.data;
            //}
        };

        // MSG BADGE COUNTER SSE
        var last_count_msg;
        var ch_notif_counter = new EventSource("ajax/get_ch_notif_counter_sse.php");
        ch_notif_counter.onmessage = function(event) {
            //if(last_count_msg){
                if(event.data != last_count_msg && event.data != ""){
                    var convo = JSON.parse(event.data);
                    var totalGcMsg = 0;
                    for(i in convo){
                        totalGcMsg += convo[i].count_convo;

                        $("#convo_badge_"+i).html(convo[i].count_convo);
                        var channel = convo[i].channels;
                        for (ch in channel) {
                            // channel count
                            $("#ch_count_"+ch).html(channel[ch]);
                        }
                    }

                    var gc_count = $("#dm_badge_pill").html();
                    var totalGC = totalGcMsg + Number(gc_count);
                    console.log(totalGC);
                    $("#total_msg_badge").html(totalGC);
                    if(totalGC > 0){
                        msg_sound_notif();
                    }

                }
                last_count_msg = event.data;
            //}
            
        };

        // REQUEST BOOK BADGE COUNTER SSE
        var last_count_rb;
        var rb_notif_counter = new EventSource("ajax/get_rb_notif_counter_sse.php");
        rb_notif_counter.onmessage = function(rb) {
            if(rb.data != last_count_rb){
                $("#total_rb_badge").html(rb.data);
            }
            last_count_rb = rb.data;
        };

        // ANNOUNCEMENT BADGE COUNTER SSE
        var notif_btn_mark = '<a href="#" class="btn btn-link btn-sm float-right" onclick="markAnnouncementAsRead()">Mark all as read</a>';
        var last_count_an;
        var an_notif_counter = new EventSource("ajax/get_an_notif_counter_sse.php");
        an_notif_counter.onmessage = function(an_event) {
            if(an_event.data != last_count_an){
                $("#total_an_badge").html(an_event.data);
                $("#announcement_badge").html(an_event.data);
                getAnnouncement();
                if(an_event.data > 0){
                    $("#mark_an_notif_as_read").html(notif_btn_mark);
                }else{
                    $("#mark_an_notif_as_read").html("");
                }
            }
            last_count_an = an_event.data;
        };

        var avatar_croppie = $('#cropper_container').croppie({
            enableExif: true,
            viewport: {
                width: 150,
                height: 150,
                type: 'circle'
            },
            boundary: {
                width: '100%',
                height: 300
            }
        });

        function closeAvatarModal(file_id) {
            $("#"+file_id).val('');
            $("#modal-crop-avatar").modal('hide');
        }
    </script>
</html>