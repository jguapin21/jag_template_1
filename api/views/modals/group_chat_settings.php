<style>
    .avatar-convo-upload {
        position: relative;
        max-width: 205px;
        margin: 0px auto;
    }

    .avatar-convo-upload .avatar-convo-edit {
        position: absolute;
        right: 0px;
        z-index: 1;
        top: 0px;
    }

    .avatar-convo-upload .avatar-convo-edit input {
        display: none;
    }

    .avatar-convo-upload .avatar-convo-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }

    .avatar-convo-upload .avatar-convo-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
    }

    .avatar-convo-upload-i {
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }

    .cr-slider-wrap {
      
    }
</style>
<div class="modal fade modal-black" id="modal-convo-settings" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 47%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="convo_title_settings">Convo Settings</h6>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-4 pb-2">
                                <div class="row">
                                    <div class="col-12" id="convo_avatar_container" style="text-align: center;">
                                        <img src="" style="width: 185px;height: 185px;object-fit: cover;" class="rounded-circle" id="cs_avatar_display">
                                    </div>
                                    <div class="avatar-convo-upload">
                                        <div class="avatar-convo-edit" style="right: 30px !important;">
                                            <input type='file' name="convo_imageUpload" id="convo_imageUpload" accept=".png, .jpg, .jpeg"/>
                                            <label for="convo_imageUpload"><i class="fas fa-pen avatar-convo-upload-i"></i></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="form-group">
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-comment-alt"></i></span>
                                        </div>
                                        <input type="text" id="settings_convo_name" class="form-control" placeholder="your convo name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="updateNewConvoName()">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 px-0 mx-0">
                        <hr class="mt-0">
                    </div>
                    <div class="col-12">
                        <h3>Delete Conversation</h3>
                        <small>All messages and all channels under this conversation will be deleted.</small><br>
                        <button type="button" class="btn btn-danger btn-sm" onclick="deleteConvo()">DELETE THIS CONVO</button>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
<script>
// CONVO CROPPIE
    function getImageUploaded(input) {
        $("#crop_btn_upload").html('');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                avatar_croppie.croppie('bind', {
                    url: e.target.result
                });
                $("#crop_btn_upload").html('<button type="button" class="btn btn-link" onclick="closeAvatarModal(\'convo_imageUpload\')">Cancel</button><button type="button" class="btn btn-success" onclick="uploadConvoAvatar()" id="cropper_apply_crop">Apply</button>');
                $("#modal-crop-avatar").modal('show');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#convo_imageUpload").change(function() {
        getImageUploaded(this);
    });
    function uploadConvoAvatar(){
        $("#cropper_apply_crop").html('Uploading...');
        avatar_croppie.croppie('result', {
            type: 'canvas',
		    size: 'viewport'
        }).then(function(response){
            $.post("ajax/upload_convo_avatar.php",{
                image: response
            },function(data){
                alertMe(data, 'success');
                $("#cs_avatar_display").attr('src', response);
                refreshChatModal();
                $("#cropper_apply_crop").html('Apply');
                $("#convo_imageUpload").val('');
                $("#modal-crop-avatar").modal('hide');
            });
        });
    }
</script>