<div class="modal fade" id="modal-about-accord" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h1>Accord</h1>
                        A simple chat system intended for humans. Organize chat by a channel serves as a vault for your messages. Accord, means to agree, to grant, to correspond to another human.<br><br>"Made by human for humans"<br>--duediewin
                    </div>
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>