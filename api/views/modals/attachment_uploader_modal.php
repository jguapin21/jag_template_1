<div class="modal fade modal-black" id="modal-upload-attachments" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 37%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">UPLOAD</h6>
            </div>
            
            <div class="modal-body" style="overflow-y: auto;">
                <div class="row">
                    <div class="col-12" style="text-align: center;">
                        <div style="height: 225px;width: 100%;">
                            <input type="hidden" id="attachment_val">
                            <img src="" style="height: 100%;object-fit: contain; border-radius: 12px;" id="gc_attachment_img">
                            <p id="file_name_bin"></p>
                        </div>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label for="gc_upload_desc">Description</label>
                            <input type="text" id="gc_upload_desc" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer" style="padding-top: 0px;">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                <span id="up_att_btn"></span>
            </div>
            
        </div>
    </div>
</div>