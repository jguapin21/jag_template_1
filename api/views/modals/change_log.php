<?php
    $getCLog = SELECT_QUERY("*","tbl_change_log_header","active = 1");
?>
<div class="modal fade modal-black" id="modal-change-log" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 45%;">
        <div class="modal-content">
            <div class="modal-body pb-0">
                <h3>What's New  <small>(<?=date("M d, Y", strtotime($getCLog[date_up]))?>)</small></h3>
            </div>
            <div class="modal-body msg_chat_scroll" style="max-height: 585px;">
                <div class="row">
                    <div class="col-12">
                       <img src="../assets/img/theme/img-1-1000x600.jpg" alt="log-banner" style="height: 205px; width: 100%;">
                       <span style="position: absolute;top: 29%;left: 0px;color: #fff;font-size: 3rem;width: 100%;text-align: center;">CHANGE LOG</span>
                    </div>
                    <div class="col-12 mt-3">
                        <div class="row">
                            <div class="col-12">
                                <h3 style="color: green;">New Features</h3>
                            </div>
                            <div class="col-12">
                                <?php
                                    $loopFT = getChangeLog();
                                    if(count($loopFT) > 0){
                                    foreach($loopFT as $ftList){
                                ?>
                                <div class="row">
                                    <div class="col" style="text-align: end;">
                                        <i class="fas fa-circle" style="color: violet;font-size: 8px;"></i>
                                    </div>
                                    <div class="col-11 pl-0">
                                        <p style="white-space: pre-wrap;"><?=$ftList["feature"]?></p>
                                    </div>
                                </div>
                                <?php } } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h3 style="color: red;">A Quick Note</h3>
                            </div>
                            <div class="col-12">
                                <p style="white-space: pre-wrap;"><?=$getCLog["quick_note"]?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" style="text-align: right;">
                                <button type="button" class="btn btn-link btn-sm" onclick="readthechangelog()">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="modal-footer">
               
            </div>
            
        </div>
    </div>
</div>