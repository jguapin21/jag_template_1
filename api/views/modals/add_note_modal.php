<div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 70%;left: 64px;top: 0px!important;margin: 0px !important;">
        <div class="modal-content" style="background:#f3f3f3;height: calc(100vh);border-radius: 0px !important;">
        	
            <div class="modal-header" style="padding-bottom: 14px;">
                <h6 class="modal-title" id="modal-title-default">Remember me! <small class="text-muted">(this will keep all your notes just for you)</small></h6>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> -->
            </div>
            
            <div class="modal-body msg_chat_scroll" style="height: 500px;">
                <div class="row">
                    <div class="col" style="display: flex;flex-direction: column;justify-content: center;align-items: baseline;">
                        <button type="button" class="btn btn-primary btn-sm" onclick="addNote()">
                            <i class="fas fa-plus"></i> 
                            Take a note
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="form-group mb-0">
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" id="notes_search_input" class="form-control" placeholder="search your notes" onkeyup="searchInNotes()">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-columns" id="note_container">
                </div>
                
            </div>
            <button type="button" class="btn btn-link" data-dismiss="modal" style="position: absolute;top: 0px;right: 20px;margin-top: 7px;">Close</button>
            
        </div>
    </div>
</div>
<script>
    function addNote() {
        $.post("ajax/add_new_note.php",{
        },function(data,status){
            $("#note_container").prepend(data);
            $("#note_title").focus();
        });
    }

    function saveNote() {
        var title = $("#note_title").val();
        var content = $("#note_content").html();
        $.post("ajax/save_note.php?op=add",{
            title: title,
            content: content
        },function(data,status){
            if(data == 1){
                alertMe('Note saved.', 'success');
            }else{
                alertMe('Error in saving your note.', 'danger');
            }
            getUserNote();
        });
    }

    function searchInNotes() {
        var search_q = $("#notes_search_input").val();
        if(search_q != ""){
            $.post("ajax/search_user_notes.php",{
                search_q: search_q
            },function(data){
                $("#note_container").html(data);
            });
        }else{
            getUserNote();
        }
    }

    function getUserNote(){
        $.post("ajax/get_user_notes.php",{
        },function(data){
            $("#note_container").html(data);
        });
    }

    function deleteNote(id) {
        var res = confirm("Are you sure you want to delete this note?");
        if(res){
            $.post("ajax/delete_note.php",{
                id: id
            },function(data){
                if(data == 1){
                    getUserNote();
                    alertMe("Note deleted", "success");
                }else{
                    alertMe("Error in deleting notes!", "danger");
                }
            });
        }
    }

    function updateNote(note_id) {
        var title = $("#update_note_title_"+note_id).val();
        var content = $("#update_note_content_"+note_id).html();
        $.post("ajax/save_note.php?op=update",{
            title: title,
            content: content,
            id: note_id
        },function(data,status){
            if(data == 1){
                alertMe('Note updated.', 'success');
            }else{
                alertMe('Error in updating your note.', 'danger');
            }
            getUserNote();
        });
    }

    function cancelNote(){
        getUserNote();
    }
</script>