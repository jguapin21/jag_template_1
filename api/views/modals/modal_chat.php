<?php
$convo = $_SESSION['chat']['convo'];
$channel = $_SESSION['chat']['channel'];
?>
<style>
    .testModal {
        position: absolute;
    }

    .chat-link-people:hover {
        padding: 0px 5px 0px 5px;
        background: #e2e2e2;
        border-radius: 6px;
    }

    .chat-link-people{
        padding: 0px 5px 0px 5px;
    }

    .show-onhover{
        display: none;
    }

    .channel_text_width{
        width: 84%;
    }

    .ch-padd-hover:hover{
        background: #747f8d3d !important;
        border-radius: 3px;
    }

    .ch-padd-hover:hover .show-onhover{
        display: block;
    }

    .ch-padd-hover:hover .channel_text_width{
        width: 72%;
    }

    .ch-padd-hover:hover .channel_display_name{
        color: #505050 !important;
    }

    .upload-media-icon input {
        display: none;
    }

    .upload-media-icon input + label {
        display: inline-block;
        margin-bottom: 0;
        cursor: pointer;
        transition: all 0.2s ease-in-out;
    }

    .show-on-msg-hover{
        display: none;
    }
    .msg_hover:hover .show-on-msg-hover {
        display: block;
        /* background: #f1f1f1;
        border-radius: 3px; */
    }

    .ch-active {
        background: #747f8d3d !important;
        border-radius: 3px;
    }

    [contentEditable=true]:empty:not(:focus):before{
        content:attr(data-text);
    }
</style>
<input type="hidden" id="session_convo_admin_hidden" value="<?=$isChatAdmin?>">
<input type="hidden" id="session_convo_id_hidden" value="<?=$_SESSION['chat']['convo']?>">
<div class="modal fade" id="modal-chat" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog testModal" role="document" style="width: 70%;left: 64px;top: 0px !important;margin: 0px !important;">
        <div class="modal-content" style="background:#fff; height: calc(100vh);border-radius: 0px !important;">
            <div class="modal-body" style="overflow: hidden;padding: 0 15px 0 15px;">
                <div class="row" style="display: flex;">
                    <div class="px-0 mx-0" style="padding-top: 9px;background: #dee2ea;height: calc(100vh); padding-bottom: 20px;">
                        <!-- Chat convos -->
                        <div class="px-2 msg_chat_scroll" style="height: 100%;width: 100%;">
                            <div class=px-0 style="text-align: center;position: relative;" id="convo_container">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <!-- GROUP CHAT -->
                            <div class="col-12" id="group_chat" style="display: contents;">
                                <?php include_once 'views/group_chat.php';?>
                            </div>
                            <!-- DIRECT CHAT  -->
                            <div class="col-12" id="direct_chat" style="display: none;">
                                <?php include_once 'views/direct_message.php';?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once 'views/modals/add_people_to_convo.php';?>
<?php include_once 'views/modals/invite_to_dm.php';?>
<?php include_once 'views/modals/channel_settings.php';?>
<?php include_once 'views/modals/group_chat_settings.php';?>
<?php include_once 'views/modals/about_accord.php';?>
<?php include_once 'views/modals/chat_user_settings.php';?>
<?php include_once 'views/modals/attachment_uploader_modal.php';?>
<?php include_once 'views/modals/msg_editor_modal.php';?>
<script>
    var allowRemoveChatMember = '<div class="col-12 px-0 mx-0"><hr class="mt-0"></div><div class="col-12"><h3>Remove from conversation</h3><small>All messages under this channel will also be deleted.</small><br><button type="button" class="btn btn-danger btn-sm" onclick="removeFromConvo()">REMOVE FROM CONVO</button></div>';

    var mc_allow_cs = '<i class="fas fa-ellipsis-v" style="cursor: pointer;font-size: 12px;padding: 5px;" onclick="convo_settings()"></i>';
    var on_click_channel_name = 0;
    var ConvoSess = $("#session_convo_admin_hidden").val();
    var start = 0;
    var dm_sse_rcp_id;

    // // Create a new WebSocket.
    // var socket = new WebSocket('ws://127.0.0.1:8000/pms3/api/ajax/socket.php');

    // // Show a connected message when the WebSocket is opened.
    // socket.onopen = function(event) {
    //     console.log('Connected to: ' + event.currentTarget.url);
    //     console.log('open');
    // };

    // // Handle any errors that occur.
    // socket.onerror = function(error) {
    //     console.log('WebSocket Error: ' + error);
    // };

    $(document).ready(function() {
        $("body").tooltip({ selector: '[data-toggle=tooltip]', container: 'body' });
        
        if(ConvoSess == 1){
            $("#link_people_settings_bin").html(allowRemoveChatMember);
            $("#mc_allow_cs").html(mc_allow_cs);
        }else{
            $("#link_people_settings_bin").html("");
            $("#mc_allow_cs").html("");
        }
    });

    function showChatOptions() {
        if($("#chat_options").css("display") == 'none'){
            $("#chat_options").css("display","block");
        }else{
            $("#chat_options").css("display","none");
        }
    }

    function deleteConvo() {
        $reslt = confirm("Are you sure you want to delete this convo?");
        if($reslt){
            $.post("ajax/delete_conv.php",{
            },function(data){
                if(data == 1){
                    refreshChatModal();
                    $("#modal-convo-settings").modal('hide');
                    alertMe("Convo deleted", 'success');
                }else{
                    alertMe("Error deleting convo", 'danger');
                }
            });
        }
    }

    function deleteChannel() {
        $reslt = confirm("Are you sure you want to delete this channel?");
        if($reslt){
            var new_channel_name = $("#settings_channel_name").val();
            var chID = $("#channel_id_"+new_channel_name).val();
            $.post("ajax/delete_channel.php",{
                chID: chID
            },function(data){
                if(data == 1){
                    refreshChatModal();
                    $("#modal-channel-settings").modal('hide');
                    alertMe("Channel deleted", 'success');
                }else{
                    alertMe("Error deleting channel", 'danger');
                }
            });
        }
    }

    function channel_settings() {
        var ch_name = $("#channel_name_hidden_edit").val();
        $("#channel_title_settings").html("#"+ch_name+" channel settings");
        $("#settings_channel_name").val(ch_name);
        $("#ch_name_hidden").val(ch_name);
        $("#modal-channel-settings").modal({
            show: true
        });
    }

    function openChannelSettings(id, name){
        $("#channel_title_settings").html("#"+name+" channel settings");
        $("#settings_channel_name").val(name);
        $("#ch_name_hidden").val(name);
        $("#modal-channel-settings").modal({
            show: true
        });
    }

    function convo_settings() {
        var old_convo_name = $("#convo_name").html();
        var convo_id = $("#session_convo_id_hidden").val();
        var convo_avatar = $("#convo_id_with_slug_"+convo_id).val();
        
        $("#settings_convo_name").val(old_convo_name);
        $("#cs_avatar_display").attr('src', convo_avatar);
        $("#modal-convo-settings").modal({
            show: true
        });
    }

    function updateNewChannelName() {
        var new_channel_name = $("#settings_channel_name").val();
        var old_channel_name = $("#ch_name_hidden").val();
        var chID = $("#channel_id_"+old_channel_name).val();
        $.post("ajax/update_channel_name.php",{
            new_channel_name: new_channel_name,
            chID: chID
        },function(data){
            if(data == 1){
                refreshChatModal();
                $("#modal-channel-settings").modal('hide');
                alertMe("Channel name updated", 'success');
            }else{
                alertMe("Error updating channel name", 'danger');
            }
        });
    }

    function updateNewConvoName(){
        var new_convo_name = $("#settings_convo_name").val();
        $.post("ajax/update_convo_name.php",{
            new_convo_name: new_convo_name
        },function(data){
            if(data == 1){
                displayConvoName();
                displayConvos();
                alertMe("Convo name updated", "success");
            }else{
                alertMe("Error updating convo name", "danger");
            }
            $("#modal-convo-settings").modal('hide');
        });
    }

    function addNewChannel() {
        $.post("ajax/add_new_channel.php",{
        },function(data){
            displayChannels();
            //$("#convo_channels").append(data);
        });
    }

    function addNewConvo() {
        $.post("ajax/add_new_convo.php",{
        },function(data){
            displayConvos();
        });
    }

    function refreshChatModal() {
        displayConvos();
        displayChannels();
        displayMsg();
        displayConvoName();
        getLinkPeople();
    }

    function refreshDmChatModal(){
        displayConvos();
        getDMLinkPeople();
    }

    function displayMsg(channel_id, channel_name){
        // $.post("ajax/get_chat_msg.php",{
        //displayChannelName();
        var msg_cloth = '', msg_list, msg_slug, isFile, media_display;
        if(channel_id > 0){
            $("#msg_contents").html("<center><h4 style='padding: 20px;'>Loading...</h4></center>");
            $.post("ajax/messages.php",{
                channel_id: channel_id
            },function(data){
                var msgdata = JSON.parse(data);
                var msg_img_basepath = "<?=MSG_ATTACHMENT_BASEPATH?>";
                // var channel_name = $("#channel_name_header").html();
                
                msg_cloth += "<div class='pl-0 pt-0 pb-0' style='width: 100%;border-bottom: 1px solid #ccc;'><h1 style='word-break: break-all;'>Welcome to #"+channel_name+"</h1><p style='word-break: break-all;line-height: 19px;'>This is the start of the #"+channel_name+" channel.</p></div>";

                for (var i = 0; i < msgdata.length; ++i) {
                    msg_list = msgdata[i];

                    msg_slug = (msg_list['attachments'].url != "")?'<div class="col pb-2" style="padding: 0px;display: grid;grid-auto-flow: row;grid-row-gap: .25rem;text-indent: 0;min-height: 0;min-width: 0;"><img src="'+msg_img_basepath+msg_list['attachments'].url+'" style="width: 60%;object-fit: contain;object-position: 0 0;border-radius: 6px;justify-self: start;align-self: start;" onclick="previewMedia(\''+msg_img_basepath+msg_list['attachments'].url+'\')"></div>':'';

                    $dl_var = "ajax/media_download.php?file_name="+msg_list['attachments'].filename;
                    isFile = '<div class="col pb-2" style="text-align: left;padding: 0px;"><div style="width: 70%;display: flex;flex-direction: row;align-items: center;align-content: center;padding: 15px;background: #f1f1f1;border-radius: 8px;"><a href="#" style="width: 35px; height: 30px;"><img src="'+msg_list['attachments'].file_extension+'" style="width: 100%;height: 100%;object-fit: cover;" data-toggle="tooltip" data-placement="left" data-original-title="" title=""></a><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;white-space: pre-wrap;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;word-break: break-all;">'+msg_list['attachments'].filename+'</h4><a href="'+$dl_var+'"><i class="fas fa-download" style="padding-left: 17px;font-size: 19px;"></i></a></div></div>';

                    media_display = (msg_list['attachments'].hasFileAttachment == 1)?isFile:msg_slug;

                    var msg_options = (msg_list.hasPriv == 1)?"<div class='show-on-msg-hover' style='position: absolute;top: 3px;right: 8px;cursor: pointer;'><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;' title='edit' onclick='edit_gc_msg("+msg_list.id+")'><i class='fa fa-pen' style='font-size: 12px;'></i></span><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;' title='delete' onclick='delete_gc_msg("+msg_list.id+","+msg_list.group_id+","+msg_list["channel"].id+")'><i class='far fa-trash-alt' style='font-size: 12px;color: red;'></i></span></div>":"";

                    var isEdited = (msg_list.edited_status == 1)?' &bull; Edited':'';

                    msg_cloth += "<div class='pl-2 pt-2 pb-0 msg_hover' style='border: 0px;width: 100%;'><div class='row'><div class='col-auto pr-0 align-items-start' style='padding-top: 3px;'><img src='"+msg_list['author'].avatar+"' style='width:40px; height: 40px;object-fit: cover;' class='rounded-circle' data-toggle='tooltip' data-placement='left' title='"+msg_list['author'].realName+"'></div><div class='col pl-3 align-items-center'><div style='text-align: start;padding: 0px;'><div class='row' style='margin: 0px;'><div class='col-12' style='display: flex;width: 100px;padding: 0px;'><div style='width: 84%;'><h3 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: bold;color: #4a4949 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;' id='channel_name'>"+msg_list['author'].username+"</h3></div>"+msg_options+"</div><div class='col-12 pl-0' style='line-height: 7px;'><small class='text-muted' style='font-size: 71% !important;'>"+msg_list.timestamp+isEdited+"</small></div></div></div><textarea hidden id='gc_msg_content_box_"+msg_list.id+"' style='font-size: 1rem;font-family: myFirstFont; word-break: break-word;white-space: pre-wrap;color: #4e4e4e;'>"+msg_list.content+"</textarea><div style='font-size: 18px;font-family: myFirstFont;word-break: break-word;white-space: pre-wrap;color: #4e4e4e; margin-bottom: 5px;line-height: 22px;-webkit-user-modify: read-only-plaintext-only;outline: -webkit-focus-ring-color auto 0px;'>"+msg_list.content+"</div>"+media_display+"</div></div></div>";
                }
                $("#msg_contents").html(msg_cloth);
                setTimeout(scrolling, 200);
            });
        }else{
            msg_cloth += "<div class='pl-0 pt-0 pb-0' style='width: 100%;border-bottom: 1px solid #ccc;'><h1 style='word-break: break-all;'>Choose channel</h1><p style='word-break: break-all;line-height: 19px;'>Start chatting someone on selected channel.</p></div>";
            $("#msg_contents").html(msg_cloth);
        }
    }

    function displayChannels(){
        $.post("ajax/get_chat_channels.php",{
        },function(data){
            var chdata = JSON.parse(data);
            var ch_cloth = '';
            var ch_list, badgeCounterShow,hid_data,chBadgeCounter,channelID,settingIcon,badge_count;

            // if(chdata == null){
            //     ch_cloth += '<div style="cursor: pointer;text-align: center;background: transparent;padding: 3px;margin: 5px 10px 10px 10px;width: -webkit-fill-available;"><span class="text-muted" style="font-family: myFirstFont;font-size: 14px;color: #32325d !important;"> Join conversation first!</span></div>';
            // }else{
                var gc_admin = $('#session_convo_admin_hidden').val();
                var isGrpAdmin = (chdata.length > 0)?chdata[0].isGroupAdmin:gc_admin;
                if(isGrpAdmin == 1){
                    ch_cloth += '<div style="cursor: pointer;text-align: center;background: #48536f;border-radius: 4px;padding: 3px;margin: 5px 10px 10px 10px;width: -webkit-fill-available;"><span class="text-muted" onclick="addNewChannel()" style="font-family: myFirstFont;font-size: 14px;color: #fff !important;">Add new channel </span></div>';
                }

                for (var i = 0; i < chdata.length; ++i) {
                    ch_list = chdata[i];

                    channelID = "channel_id_"+ch_list["channel"].name;
                    chBadgeCounter = "ch_count_"+ch_list["channel"].id;
                    badge_count = (ch_list["channel"].unread_counter == null)?'':ch_list["channel"].unread_counter;
                    badgeCounterShow = '<span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;" id="'+chBadgeCounter+'">'+badge_count+'</span>';
                    hid_data = "<input type='hidden' id='"+channelID+"' value='"+ch_list["channel"].id+"'>";
                    settingIcon = (isGrpAdmin == 1)?'<i class="fas fa-cog show-onhover" style="cursor: pointer;font-size: 12px;padding: 5px;" onclick="openChannelSettings(\''+ch_list["channel"].id+'\',\''+ch_list["channel"].name+'\')"></i>':'';

                    ch_cloth += '<div class="ch-padd-hover px-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;border: 0px !important;width: -webkit-fill-available;background: transparent;margin-bottom: 2px;">'+hid_data+'<div onclick="get_channel_msgs(\''+ch_list["channel"].id+'\',\''+ch_list["channel"].name+'\')" class="channel_text_width"><h4 class="text-muted channel_display_name" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;padding: 5px;"># '+ch_list["channel"].name+'</h4></div><div style="align-items: baseline;justify-content: center;display: flex;">'+badgeCounterShow+settingIcon+'</div></div>';

                    if(chdata[0].isGroupAdmin == 1){
                        on_click_channel_name = 1;
                    }else{
                        on_click_channel_name = 0;
                    }
                }
            // }

           $("#convo_channels").html(ch_cloth);
        });
    }

    function get_sender_detail(id, name, user_avatar){
        $('#selected_user_avatar').val(user_avatar);
        clearDMnotif(id);
        dm_get_msg(id); 
        $("#dm_recepient_id").val(id);
        $("#dm_recepient_name").html(name);
    }

    function dm_get_msg(id){
        dm_sse_rcp_id = id;
        $.post("ajax/get_dm_messages.php",{
            id: id
        },function(data){
            var dmmsgdata = JSON.parse(data);
            var dm_name_rcpnt = $("#dm_recepient_name").html();
            var msg_img_basepath = "<?=MSG_ATTACHMENT_BASEPATH?>";
            var dm_msg_cloth = '', dm_msg_list, dm_msg_slug, dm_isFile, dm_msg_options, dm_media_display, dm_isEdited;
            var show_first = 1;
            var slctd_user_avatar = $('#selected_user_avatar').val();

            if(dm_name_rcpnt == ""){
                dm_msg_cloth += "<div class='pl-0 pt-1 pb-0' style='width: 100%;border-bottom: 1px solid #ccc;'><p style='word-break: break-all;line-height: 19px;'>DM a friend now!</p></div>";
            }else{
                dm_msg_cloth += "<div class='pl-0 pt-1 pb-0' style='width: 100%;border-bottom: 1px solid #ccc;'><img src='"+slctd_user_avatar+"' style='width: 120px;height: 120px;object-fit: cover;' class='rounded-circle'><h1 style='word-break: break-all;margin-bottom: 5px;'>"+dm_name_rcpnt+"</h1><p style='word-break: break-all;line-height: 19px;'>Your history starts here with @"+dm_name_rcpnt+".</p></div>";

                if(dmmsgdata.length > 0){
                    for (var i = 0; i < dmmsgdata.length; ++i) {
                        dm_msg_list = dmmsgdata[i];

                        dm_msg_slug = (dm_msg_list['attachments'].url != "")?'<div class="col pb-2" style="padding: 0px;display: grid;grid-auto-flow: row;grid-row-gap: .25rem;text-indent: 0;min-height: 0;min-width: 0;"><img src="'+msg_img_basepath+dm_msg_list['attachments'].url+'" style="width: 60%;object-fit: contain;object-position: 0 0;border-radius: 6px;justify-self: start;align-self: start;" onclick="previewMedia(\''+msg_img_basepath+dm_msg_list['attachments'].url+'\')"></div>':'';

                        dm_dl_var = "ajax/media_download.php?file_name="+dm_msg_list['attachments'].filename;
                        dm_isFile = '<div class="col pb-2" style="text-align: left;padding: 0px;"><div style="width: 70%;display: flex;flex-direction: row;align-items: center;align-content: center;padding: 15px;background: #f1f1f1;border-radius: 8px;"><a href="#" style="width: 35px; height: 30px;"><img src="'+dm_msg_list['attachments'].file_extension+'" style="width: 100%;height: 100%;object-fit: cover;" data-toggle="tooltip" data-placement="left" data-original-title="" title=""></a><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;white-space: pre-wrap;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;word-break: break-all;">'+dm_msg_list['attachments'].filename+'</h4><a href="'+dm_dl_var+'"><i class="fas fa-download" style="padding-left: 17px;font-size: 19px;"></i></a></div></div>';

                        dm_media_display = (dm_msg_list['attachments'].hasFileAttachment == 1)?dm_isFile:dm_msg_slug;

                        dm_msg_options = (dm_msg_list.hasPriv == 1)?"<div class='show-on-msg-hover' style='position: absolute;top: 3px;right: 8px;cursor: pointer;'><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;' title='edit' onclick='edit_dm_msg("+dm_msg_list.id+")'><i class='fa fa-pen' style='font-size: 12px;'></i></span><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;' title='delete' onclick='delete_dm_msg("+dm_msg_list.id+","+dm_msg_list.group_id+","+dm_msg_list["recipient"].id+")'><i class='far fa-trash-alt' style='font-size: 12px;color: red;'></i></span></div>":"";

                        dm_isEdited = (dm_msg_list.edited_status == 1)?' &bull; Edited':'';

                        dm_msg_cloth += "<div class='pl-2 pt-2 pb-0 msg_hover' style='border: 0px;width: 100%;'><div class='row'><div class='col-auto pr-0 align-items-start' style='padding-top: 3px;'><img src='"+dm_msg_list['author'].avatar+"' style='width:40px; height: 40px;object-fit: cover;' class='rounded-circle' data-toggle='tooltip' data-placement='left' title='"+dm_msg_list['author'].username+"'></div><div class='col pl-3 align-items-center'><div style='text-align: start;padding: 0px;'><div class='row' style='margin: 0px;'><div class='col-12' style='display: flex;width: 100px;padding: 0px;'><div style='width: 84%;'><h3 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: bold;color: #4a4949 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;' id='dm_auth_name'>"+dm_msg_list['author'].username+"</h3></div>"+dm_msg_options+"</div><div class='col-12 pl-0' style='line-height: 7px;'><small class='text-muted' style='font-size: 71% !important;'>"+dm_msg_list.timestamp+dm_isEdited+"</small></div></div></div><textarea hidden id='dm_msg_content_box_"+dm_msg_list.id+"' style='font-size: 1rem;font-family: myFirstFont; word-break: break-word;white-space: pre-wrap;color: #4e4e4e;'>"+dm_msg_list.content+"</textarea><div style='font-size: 18px;font-family: myFirstFont;word-break: break-word;white-space: pre-wrap;color: #4e4e4e; margin-bottom: 5px;line-height: 22px;-webkit-user-modify: read-only-plaintext-only;outline: -webkit-focus-ring-color auto 0px;'>"+dm_msg_list.content+"</div>"+dm_media_display+"</div></div></div>";

                        show_first++;
                    }
                    var _arr_last_ = dmmsgdata.pop();
                    start = _arr_last_.id;
                }
            }
            $("#dm_msg_contents").html(dm_msg_cloth);
            setTimeout(dm_msg_scrolling, 200);
        });
    }

    function chat_session_updater(id, type){
        if(type == "convo"){
            $("#session_convo_id_hidden").val(id);
            $.post("ajax/set_chat_session.php",{
                id: id,
                type: type
            },function(data){
                $("#session_convo_admin_hidden").val(data);
                if(data == 1){
                    $("#link_people_settings_bin").html(allowRemoveChatMember);
                    $("#mc_allow_cs").html(mc_allow_cs);
                }else{
                    $("#link_people_settings_bin").html("");
                    $("#mc_allow_cs").html("");
                }
                refreshChatModal();
                $("#channel_name_header").html('');
                $("#direct_chat").css("display","none");
                $("#group_chat").css("display","contents");
            });
        }else if(type == "dm"){
            $.post("ajax/set_chat_session.php",{
                id: id,
                type: type
            },function(data){
                refreshDmChatModal();
                $("#direct_chat").css("display","contents");
                $("#group_chat").css("display","none");
            });
        }else{
            clearMyNotif(id);
            $.post("ajax/set_chat_session.php",{
                id: id,
                type: type
            },function(data){
                $("#session_convo_admin_hidden").val(data);
                if(data == 1){
                    $("#link_people_settings_bin").html(allowRemoveChatMember);
                    $("#mc_allow_cs").html(mc_allow_cs);
                }else{
                    $("#link_people_settings_bin").html("");
                    $("#mc_allow_cs").html("");
                }
                displayChannelName();
                refreshChatModal();
                $("#direct_chat").css("display","none");
                $("#group_chat").css("display","contents");
            });
        }
    }

    function get_channel_msgs(id, cahnnel_name){
        $.post("ajax/set_chat_session.php",{
            id: id,
            type: 'channel'
        },function(data){
            $("#session_convo_admin_hidden").val(data);
            if(data == 1){
                $("#link_people_settings_bin").html(allowRemoveChatMember);
                $("#mc_allow_cs").html(mc_allow_cs);
            }else{
                $("#link_people_settings_bin").html("");
                $("#mc_allow_cs").html("");
            }

            clearMyNotif(id);
            displayMsg(id, cahnnel_name); 
            $("#channel_name_header").html(cahnnel_name);
        });
    }

    function displayConvoName() {
        var type = "convo";
        $.post("ajax/get_chat_ui_name.php",{
            type: type
        },function(data){
            if(data == ""){
                setText = "Accord Chat";
            }else{
                setText = data;
            }
           $("#convo_name").html(setText);
        });
    }

    function displayChannelName() {
        var type = "channel";
        var setText;
        $.post("ajax/get_chat_ui_name.php",{
            type: type
        },function(data){
            if(data === ""){
                setText = "Conversation";
            }else{
                setText = data;
            }
           $("#channel_name_header").html(setText);
           $("#channel_name_hidden_edit").val(data);
        });
    }

    function getLinkPeople() {
        $.post("ajax/get_link_people.php",{
        },function(data){
            var memberdata = JSON.parse(data);
            var member_cloth = '', isOnline, owner, allowUpdateNickName, mem_list;
            var hddn_admin_checker = $('#session_convo_admin_hidden').val();
            var isAdmin = (memberdata.length > 0)?memberdata[0].isChatAdmin:hddn_admin_checker;
           
            if(memberdata != null){
                if(isAdmin == 1){
                    member_cloth += '<div style="cursor: pointer;text-align: center;background: #48536f;border-radius: 4px;padding: 3px;margin: 10px;width: -webkit-fill-available;"><span class="text-muted" onclick="addPeopleToConvo()" style="font-family: myFirstFont;font-size: 14px;color: #fff !important;">Invite a friend</span></div>';
                }

                for (var i = 0; i < memberdata.length; ++i) {
                    mem_list = memberdata[i];

                    isOnline = '<div class="status-circle" style="width: 18px;height: 15px;border-radius: 50%;background-color: grey;border: 2px solid white;top: 12px;right: 14px;position: relative;"></div>';

                    owner = (mem_list.people.hasCrown == 1)?'<div class="status-circle" style="width: 18px;height: 15px;border-radius: 50%;background-color: green;border: 2px solid white;top: 12px;right: 14px;position: relative;"><i class="fas fa-crown" style="color: orange;font-size: 8px;position: absolute;top: 1px;right: 0px;"></i></div>':isOnline;

                    allowUpdateNickName = (isAdmin == 1)?'onclick="openChatUserSettings(\''+mem_list.people.id+'\',\''+mem_list.people.nickName+'\')"':(mem_list.people.canChangeNickName == 1)?'onclick="openChatUserSettings(\''+mem_list.people.id+'\',\''+mem_list.people.nickName+'\')"':"";;

                    member_cloth += '<div class="ch-padd-hover mb-1 mt-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;padding: 5px;border: 0px !important;width: -webkit-fill-available;opacity: 0.5;" '+allowUpdateNickName+'><div style="width: 100%;display: flex;justify-content: center;flex-direction: row;align-items: center;align-content: center;"><img src='+mem_list.people.avatar+' style="width: 35px; height: 35px;object-fit: cover;" class="avatar rounded-circle" data-toggle="tooltip" data-placement="left">'+owner+'<h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: bold;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 0px;">'+mem_list.people.nickName+'</h4></div></div>';
                }
            }
            $("#convo_link_people").html(member_cloth);
        });
    }

    function getDMLinkPeople() {
        $.post("ajax/get_dm_link_people.php",{
        },function(data){
            var dmdata = JSON.parse(data);
            var dm_cloth = ''
            var dm_list;

            dm_cloth += '<div style="cursor: pointer;text-align: center;background: #48536f;border-radius: 4px;padding: 3px;margin: 10px;width: -webkit-fill-available;" onclick="addPeopleToDm()"><span class="text-muted" style="font-family: myFirstFont;font-size: 14px;color: #fff !important;">Find a friend to DM</span></div>';

            for (var i = 0; i < dmdata.length; ++i) {
                dm_list = dmdata[i];

                dm_cloth += '<div class="ch-padd-hover mb-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;padding: 3px;border: 0px !important;width: -webkit-fill-available;" onclick="get_sender_detail(\''+dm_list["recepient"].id+'\', \''+dm_list["recepient"].name+'\', \''+dm_list["recepient"].avatar+'\')"><div style="width: 85%;display: flex;justify-content: center;flex-direction: row;align-items: center;align-content: center;padding: 3px 5px 3px 5px;"><img src='+dm_list["recepient"].avatar+' style="width: 25px; height: 25px;object-fit: cover;" class="avatar rounded-circle" data-toggle="tooltip" data-placement="left"><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: bold;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;">'+dm_list["recepient"].name+'</h4></div><div style="position: relative;top: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 0px solid #fff;" id="rcp_badge_pill_'+dm_list["recepient"].id+'">'+dm_list.badge_count+'</span></div></div>';
            }

            $("#dm_convo_link_people").html(dm_cloth);
        });
    }

    function displayConvos(){
        $.post("ajax/get_chat_convos.php",{
        },function(data){
            var convo_data = JSON.parse(data);
            var convo_cloth = '', convo_list;
            var convo_icon = "<?=CONVO_AVATAR_BASEPATH?>";

            var counter_dm = (convo_data.length > 0)?(convo_data[0].dm_unread_counter > 0)?'<span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #dee2ea;" id="dm_badge_pill">'+convo_data[0].dm_unread_counter+'</span>':'<span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #dee2ea;" id="dm_badge_pill"></span>':'';
            
            convo_cloth += '<div class="col px-0" title="Direct Message" style="transition: all 0s ease 0s;" onclick="chat_session_updater(\'-1\',\'dm\')"><a href="#" class="avatar rounded-circle mb-0 mt-1" style="width: 45px;height: 45px;"><i class="fas fa-comment-alt"></i></a><div style="position: absolute;bottom: 0px;right: 0px;">'+counter_dm+'</div></div><hr class="mx-2 my-2">';

            for (var i = 0; i < convo_data.length; ++i) {
                convo_list = convo_data[i];

                convo_cloth += '<div class="col px-0" title="'+convo_list.group.name+'" style="transition: 	all 0s ease 0s;margin-bottom: 7px;" onclick="chat_session_updater(\''+convo_list.group.id+'\',\'convo\')">'+convo_list.group.avatar+'<div style="position: absolute;bottom: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #dee2ea;" id="convo_badge_'+convo_list.group.id+'">'+convo_list.group.unread_counter+'</span></div></div>';

                convo_cloth += "<input type='hidden' value='"+convo_icon+convo_list.group.slug+"' id='convo_id_with_slug_"+convo_list.group.id+"'>";
            }

            convo_cloth += '<div class="col px-0" title="New Group" style="transition: all 0s ease 0s;" onclick="addNewConvo()"><a href="#" class="avatar rounded-circle mb-0 mt-1" style="width: 45px;height: 45px;border: 2px dashed green;background-color: #d4d8dc;"><i class="fas fa-plus" style="color: green;"></i></a><div style="position: absolute;bottom: 0px;right: 0px;"></div></div>';

           $("#convo_container").html(convo_cloth);
           //$("#dm_convo_container").html(data);
        });
    }

    $('#send_msg_content').on('keydown', function (e) {
        if (e.shiftKey && e.keyCode == 13) {
           
        }else if(e.keyCode == 13){
            postMsg();
        }
        $('#send_msg_content').focus();
    });

    $('#send_dm_msg_content').on('keydown', function (e) {
        if (e.shiftKey && e.keyCode == 13) {
           
        }else if(e.keyCode == 13){
            postDmMsg();
        }
        $('#send_dm_msg_content').focus();
    });


    function postMsg() {
        var msg = $("#send_msg_content").html();
        if(msg.trim() === "" || msg.trim() === " "){
            $('#send_msg_content').html('');
        }else{
            // socket.send(msg);
            $.post("ajax/post_chat_msg.php",{
                msg: msg
            },function(data){
                $('#send_msg_content').html('');
                if(data == 3){
                    alertMe("Join convo and channel first!", 'warning'); 
                }
                // // msg_sound_notif();
            });
        }
    }

    function postDmMsg() {
        var msg = $("#send_dm_msg_content").html();
        var receiver_id = $("#dm_recepient_id").val();
        
        if(msg.trim() === "" || msg.trim() === " "){
            $('#send_dm_msg_content').html('');
        }else{
            $.post("ajax/post_dm_msg.php",{
                msg: msg,
                receiver_id: receiver_id
            },function(data){
                if(data == 3){
                    alertMe("Join convo and channel first!", 'warning'); 
                }
                $('#send_dm_msg_content').html('');
                // msg_sound_notif();
            });
        }
    }

    function addPeopleToConvo() {
        $("#convo-search-people").val('');
        searchUser();
        $("#modal-add-people-to-convo").modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
        $("#convo-search-people").focus();
    }

    function addPeopleToDm(){
        $("#convo-dm-search-people").val('');
        searchDmUser();
        $("#modal-add-people-to-dm").modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
        $("#convo-dm-search-people").focus();
    }

    function inviteDmPeople(sender_id, user_avatar, isOnline, member_name) {

        var data = '<div class="ch-padd-hover mb-1" style="display: flex;align-items: center;cursor: pointer;justify-content: space-between;padding: 3px;border: 0px !important;width: -webkit-fill-available;" onclick="get_sender_detail(\''+sender_id+'\', \''+member_name+'\', \''+user_avatar+'\')"><div style="width: 85%;display: flex;justify-content: center;flex-direction: row;align-items: center;align-content: center;padding: 3px 5px 3px 5px;"><img src='+user_avatar+' style="width: 25px; height: 25px;object-fit: cover;" class="avatar rounded-circle" data-toggle="tooltip" data-placement="left"><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: bold;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;">'+member_name+'</h4></div><div style="position: relative;top: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;" id="rcp_badge_pill_'+sender_id+'"></span></div></div>';

        get_sender_detail(sender_id, member_name, user_avatar);
        $("#dm_convo_link_people").append(data);
        $("#modal-add-people-to-dm").modal('hide');
    }

    $('#convo-dm-search-people').on('keypress', function (e) {
        if(e.keyCode == 13){
            searchDmUser();
        }
        $('#convo-dm-search-people').focus();
    });

    function searchDmUser() {
        var search_q = $("#convo-dm-search-people").val();
        $.post("ajax/dm_search_people.php",{
            search_q: search_q
        },function(data){
            if(data != 1){
                $("#dm-search-result").html(data);
            }else{
                alertMe("Please enter a valid email address.","danger");
            }
        });
    }

    function aboutAccord() {
        $("#modal-about-accord").modal({
            show: true
        });
    }
    
    function openChatUserSettings(member_id, member_name) {
        $("#settings_chat_nickname").val(member_name);
        $("#user_memberid_hidden").val(member_id);
        $("#modal-chat-user-settings").modal({
            show: true
        });
    }

    function updateNewNickName() {
        var new_nickname = $("#settings_chat_nickname").val();
        var member_id = $("#user_memberid_hidden").val();
        $.post("ajax/save_user_nickname.php",{
            new_nickname: new_nickname,
            member_id: member_id
        },function(data){
            if(data == 1){
                getLinkPeople();
                displayMsg();
                $("#modal-chat-user-settings").modal('hide');
                alertMe("Nickname updated", "success"); 
            }else{
                alertMe("Error updating nickname!", "danger"); 
            }
        });
    }

    function removeFromConvo() {
        $reslt = confirm("Are you sure you want to remove this human?");
        if($reslt){
            var member_id = $("#user_memberid_hidden").val();
            $.post("ajax/chat_member_removal.php",{
                member_id: member_id
            },function(data){
                if(data == 1){
                    getLinkPeople();
                    displayMsg();
                    $("#modal-chat-user-settings").modal('hide');
                    alertMe("Person removed", "success"); 
                }else{
                    alertMe("Error removing person!", "danger"); 
                }
            });
        }
    }

    function clearMyNotif(id) {
        $.post("ajax/clear_my_chat_notif.php",{
            id: id
        },function(data){
            displayConvos();
            displayChannels();
            $("#total_msg_badge").html(data);
        });
    }

    function clearDMnotif(rcp_id) {
        $.post("ajax/clear_dm_notif.php",{
            rcp_id: rcp_id
        },function(data){
            var newCount = data.split(":");
            $("#dm_badge_pill").html(newCount[0]);
            $("#total_msg_badge").html(newCount[1]);
            $("#rcp_badge_pill_"+rcp_id).html('');
        });
    }

    // UPLOAD MEDIA ATTACHMENT
    function gc_up_attachments(input) {
        if (input.files && input.files[0]) {
            $("#file_name_bin").html('');
            $("#up_att_btn").html('');
            $("#attachment_val").val('');
            $("#gc_attachment_img").attr('src', '');
            var reader = new FileReader();
            reader.onload = function(gc_attach_e) {
                $("#attachment_val").val(gc_attach_e.target.result);
                $("#gc_attachment_img").attr('src', gc_attach_e.target.result);
                $("#up_att_btn").html('<button type="button" class="btn btn-success" onclick="upload_gc_attachment()" id="upload_file_btn">Upload</button>');
                $("#modal-upload-attachments").modal('show');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#gchat_upload_media").change(function() {
        gc_up_attachments(this);
    });

    function upload_gc_attachment(){
        var attachment_val = $("#attachment_val").val();
        var msg = $("#gc_upload_desc").val();
        var type = 'GC';
        $("#upload_file_btn").html("<span class='fa fa-spin fa-spinner'></span> uploading...");
        $.post("ajax/post_gc_attachment.php",{
            attachment_val: attachment_val,
            msg: msg,
            type: type
        },function(data){
            if(data == 1){
                $("#gc_upload_desc").val('');
                $("#modal-upload-attachments").modal('hide');
                $("#up_att_btn").html('');
                $("#attachment_val").val('');
            }else if(data == 3){
                alertMe("Join convo and channel first!", "warning"); 
            }else{
                alertMe("Error uploading media", "danger");
            }
            $("#upload_file_btn").html("Upload");
        });
    }

    // UPLOAD FILE ATTACHMENTS
    function gc_file_up_attachments(input) {
        if (input.files && input.files[0]) {
            $("#up_att_btn").html('');
            $("#attachment_val").val('');
            $("#file_name_bin").html('');
            $("#gc_attachment_img").attr('src', '');
            var fileName = input.files[0].name;
            var extension = input.files[0].name.split('.').pop().toUpperCase();
            var reader = new FileReader();
            reader.onload = function(gc_file_attach_e) {
                var icon;
                var icon_basepath = '<?=FILE_ATTACHMENT_BASEPATH?>';
                var icons_array = ["XLS", "DOCX", "CSV", "TXT", "ZIP", "EXE", "XLSX", "PPT", "PPTX"];
                var isExtionHasIcon = icons_array.includes(extension);
                if(isExtionHasIcon){
                    icon = icon_basepath+extension+'.png';
                }else{
                    icon = icon_basepath+'FILE.png';
                }

                $("#attachment_val").val(gc_file_attach_e.target.result);
                $("#gc_attachment_img").attr('src', icon);
                $("#file_name_bin").html(fileName);
                $("#up_att_btn").html('<button type="button" class="btn btn-success" onclick="upload_gc_file_attachment()" id="upload_file_btn">Upload</button>');
                $("#modal-upload-attachments").modal('show');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#gchat_upload_file").change(function() {
        gc_file_up_attachments(this);
    });
    
    function upload_gc_file_attachment(){
        var attachment_val = $("#attachment_val").val();
        var msg = $("#gc_upload_desc").val();
        var filename = $("#file_name_bin").html();
        var type = 'GC';
        $("#upload_file_btn").html("<span class='fa fa-spin fa-spinner'></span> uploading...");
        $.post("ajax/post_gc_file_attachment.php",{
            attachment_val: attachment_val,
            msg: msg,
            type: type,
            filename: filename
        },function(data){
            if(data == 1){
                $("#gc_upload_desc").val('');
                $("#modal-upload-attachments").modal('hide');
                $("#up_att_btn").html('');
                $("#attachment_val").val('');
            }else if(data == 3){
                alertMe("Join convo and channel first!", "warning"); 
            }else{
                alertMe("Error uploading media", "danger");
            }
            $("#upload_file_btn").html("Upload");
        });
    }


    // DM UPLOAD ATTACHMENT
    function dm_up_attachments(input) {
        if (input.files && input.files[0]) {
            $("#up_att_btn").html('');
            $("#attachment_val").val('');
            $("#gc_attachment_img").attr('src', '');
            var reader = new FileReader();
            reader.onload = function(gc_attach_e) {
                $("#file_name_bin").val('');
                $("#attachment_val").val(gc_attach_e.target.result);
                $("#gc_attachment_img").attr('src', gc_attach_e.target.result);
                $("#up_att_btn").html('<button type="button" class="btn btn-success" onclick="upload_dm_attachment()" id="upload_dm_file_btn">Upload</button>');
                $("#modal-upload-attachments").modal('show');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#dm_upload_media").change(function() {
        dm_up_attachments(this);
    });

    function upload_dm_attachment(){
        var attachment_val = $("#attachment_val").val();
        var msg = $("#gc_upload_desc").val();
        var recpient_id = $("#dm_recepient_id").val();
        var type = 'DM';
        $("#upload_dm_file_btn").html("<span class='fa fa-spin fa-spinner'></span> uploading...");
        $.post("ajax/post_gc_attachment.php",{
            attachment_val: attachment_val,
            msg: msg,
            type: type,
            recpient_id: recpient_id
        },function(data){
            if(data == 1){
                $("#gc_upload_desc").val('');
                $("#modal-upload-attachments").modal('hide');
                $("#up_att_btn").html('');
                $("#attachment_val").val('');
            }else if(data == 3){
                alertMe("Join convo and channel first!", "warning"); 
            }else{
                alertMe("Error uploading media", "danger");
            }
            $("#upload_dm_file_btn").html("Upload");
        });
    }


    function delete_gc_msg(msg_id, convo_id, channel_id){
        var res = confirm("Are you sure you want to delete this msg?");
        if(res){
            $.post("ajax/delete_gc_msg.php",{
                msg_id: msg_id,
                convo_id: convo_id,
                channel_id: channel_id
            },function(data){
                displayMsg();
            });
        }
    }

    function delete_dm_msg(msg_id, convo_id, sender_id){
        var res = confirm("Are you sure you want to delete this msg?");
        if(res){
            $.post("ajax/delete_dm_msg.php",{
                msg_id: msg_id,
                convo_id: convo_id,
                sender_id: sender_id
            },function(data){
                var rcp_id = $("#dm_recepient_id").val();
                dm_get_msg(rcp_id);
                alertMe('Message Deleted', 'success');
            });
        }
    }

    function edit_gc_msg(id) {
        $("#up_edit_btn").html('');
        $("#msg_edit_txt").val('');
        var msg = $("#gc_msg_content_box_"+id).val();
        $("#up_edit_btn").html('<button type="button" class="btn btn-success" onclick="save_edited_msg('+id+')">Save changes</button>');
        $("#msg_edit_txt").val(msg);
        $("#modal-edit-message").modal({
            show: true
        });
    }

    function save_edited_msg(id) {
        var res = confirm("Are you sure you want to edit this msg?");
        if(res){
            var new_msg = $("#msg_edit_txt").val();
            if(new_msg.trim() === "" || new_msg.trim() === " "){
            }else{
                $.post("ajax/save_edited_message.php",{
                    msg_id: id,
                    new_msg: new_msg
                },function(data){
                    displayMsg();
                    $("#modal-edit-message").modal('hide');
                });
            }
        }
    }

    function edit_dm_msg(id) {
        $("#up_edit_btn").html('');
        $("#msg_edit_txt").val('');
        var msg = $("#dm_msg_content_box_"+id).val();
        $("#up_edit_btn").html('<button type="button" class="btn btn-success" onclick="save_edited_dm_msg('+id+')">Save changes</button>');
        $("#msg_edit_txt").val(msg);
        $("#modal-edit-message").modal({
            show: true
        });
    }

    function save_edited_dm_msg(id) {
        var res = confirm("Are you sure you want to edit this msg?");
        if(res){
            var new_msg = $("#msg_edit_txt").val();
            if(new_msg.trim() === "" || new_msg.trim() === " "){
            }else{
                $.post("ajax/save_edited_message.php",{
                    msg_id: id,
                    new_msg: new_msg
                },function(data){
                    var rcp_id = $("#dm_recepient_id").val();
                    dm_get_msg(rcp_id);
                    $("#modal-edit-message").modal('hide');
                });
            }
        }
    }

    $("#channel_name_header").click(function(){
        if(on_click_channel_name == 1){
            channel_settings();
        }
    });

    $('#send_dm_msg_content').click(function () {
        var dmRcpId = $("#dm_recepient_id").val();
        clearDMnotif(dmRcpId);
    });

    $('#send_msg_content').on('focus', function(){
        var isFocused = $(this).hasClass('pastable-focus');
    }).pastableContenteditable().on('blur', function(){
        var isFocused = $(this).hasClass('pastable-focus');
    });

    $('#send_msg_content').on('pasteImage', function(ev, data) {
        var blobUrl = URL.createObjectURL(data.blob);
        var targetRes = data.dataURL;
        $("#attachment_val").val(targetRes);
        $("#gc_attachment_img").attr('src', targetRes);
        $("#up_att_btn").html('<button type="button" class="btn btn-success" onclick="upload_gc_attachment()" id="upload_file_btn">Upload</button>');
        $("#modal-upload-attachments").modal('show');
    });

    // CHAT MESSAGE SSE
    var chat_source = new EventSource("ajax/get_msg_sse.php");
    chat_source.onopen = function() {
        var last_msg_data = "";
        chat_source.onmessage = function(chat_msg_event) {
            var msgsID = chat_msg_event.data;
            if(msgsID != last_msg_data && msgsID != ""){
                $.post("ajax/get_chat_msg_by_sse.php",{
                    msg_id: msgsID
                },function(data){
                    $("#msg_contents").append(data);
                    scrolling();
                });
            }
            last_msg_data = chat_msg_event.data;
        };
    };

    // CHAT DM MESSAGE SSE
    var dm_source = new EventSource("ajax/get_dm_chat_msg.php");
    dm_source.onopen = function() {
        dm_source.onmessage = function(dm_msg_event) {
            var DMmsgsID = dm_msg_event.data;
            if(DMmsgsID != start && DMmsgsID != ""){
                $.post("ajax/get_dm_msg_by_sse.php",{
                    msg_id: DMmsgsID,
                    rcp_id: dm_sse_rcp_id
                },function(data){
                    $("#dm_msg_contents").append(data);
                    dm_msg_scrolling();
                });
            }
            dm_sse_rcp_id = $("#dm_recepient_id").val();
            start = DMmsgsID;
        };
    };
    
</script>