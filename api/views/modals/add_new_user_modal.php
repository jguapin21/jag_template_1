<div class="modal fade modal-black" id="add-new-user-modal" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
            <div class="modal-body pb-0">
                <h3>Hello, got a task? good choice!</h3>
            </div>
            <div class="modal-body msg_chat_scroll" style="max-height: 585px;">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                            </div>
                            <input class="form-control" placeholder="Complete name" type="text" id="opt_name" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                            </div>
                            <input class="form-control" placeholder="Username" type="text" id="opt_username" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input class="form-control" placeholder="Password" type="password" id="opt_password" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                            </div>
                            <input class="form-control" placeholder="Email" type="email" id="opt_email" autocomplete="off">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="createAccount()">Create Account</button>
            </div>
            
        </div>
    </div>
</div>