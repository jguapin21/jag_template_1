<style>
.rb_data {
    width: 105px !important;
    vertical-align: baseline;
    font-weight: 800;
}

.rb_desc {
    white-space: pre-wrap;
}

.rb_mid {
    vertical-align: baseline;
    font-weight: 800;
}
</style>
<div class="modal fade" id="modal-request-book" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 37%;left: 64px;top: 0px!important;margin: 0px !important;">
        <div class="modal-content" style="background:#f3f3f3;height: calc(100vh);border-radius: 0px !important;">
        	
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Request book</h6>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> -->
            </div>
            
            <div class="modal-body pt-0 pb-0">
                <div class="row pb-2">
                    <div class="col" style="display: flex;flex-direction: column;justify-content: center;align-items: baseline;">
                        <button type="button" class="btn btn-primary btn-sm" onclick="addRequestLog()">
                            <i class="fas fa-plus"></i> 
                            Log a request
                        </button>
                    </div>
                    <div class="col-8">
                        <div class="form-group mb-0">
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" id="rb_search_input" class="form-control" placeholder="search log book" onkeyup="searchInLogs()">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row msg_chat_scroll" id="request_log_container" style="height: calc(73vh);">
                   
                </div>
                
            </div>
            
            <div class="modal-footer pt-0">
            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
<?php include_once 'views/modals/log_a_request.php';?>
<?php include_once 'views/modals/approve_request_remark.php';?>
<script>
    
    function addRequestLog() {
        $("#modal-log-request").modal('show');
    }

    function saveNewRequestBook() {
        var requestedBy = $("#rb_requested_by").val();
        var description = $("#rb_description").val();
        if(requestedBy == "" || description == ""){
            alertMe("please fill all fields", "warning");
        }else{
            $.post("ajax/save_request_log.php",{
                requestedBy: requestedBy,
                description: description
            },function(data){
                if(data == 1){
                    alertMe('Request saved', "success");
                    $("#modal-log-request").modal('hide');
                }else{
                    alertMe('Error in saving your request.', 'danger');
                }
                getRequestBookData();
            });
        }
    }

    function getRequestBookData() {
        $.post("ajax/get_request_book_data.php",{
        },function(data){
            $("#request_log_container").html(data);
        });
    }

    function searchInLogs(){
        var searchq = $("#rb_search_input").val();
        if(searchq != ""){
            $.post("ajax/search_log_book.php",{
                searchq: searchq
            },function(data){
                $("#request_log_container").html(data);
            });
        }else{
            getRequestBookData();
        }
    }

    function approveRequest(requestid) {
        $("#rb_request_id").val(requestid);
        $("#modal-approve-remarks").modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
    }

    function deleteRequest(id) {
        var res = confirm("Are you sure you want to delete this request?");
        if(res){
            $.post("ajax/delete_log_request.php",{
                id: id
            },function(data){
                getRequestBookData();
            });
        }
    }

    function completeApprovalRequest() {
        var request_id = $("#rb_request_id").val();
        var rb_remarks = $("#rb_remarks").val();
        $.post("ajax/approve_log_request.php",{
            request_id: request_id,
            rb_remarks: rb_remarks
        },function(data){
            $("#modal-approve-remarks").modal('hide');
            getRequestBookData();
        });
    }
</script>