<!-- Chat channels -->
<div class="col-3 pt-0 pl-0 pr-0 pb-3" style="background: #eaeaea;display: flex;flex-direction: column;border-left: 0px solid #ded9d9;height: calc(100vh);" id="dm_convo_channel_container">                
    <div style="cursor: pointer;text-align: start;padding: 0px;border-bottom: 1px solid #dedede;background: #eaeaea;">
        <div class="row" style="margin: 8px;">
            <div class="col-12" style="display: flex;width: 100px;padding: 0px;">
                <h3 class="text-muted mb-0" style="font-family: myFirstFont;font-weight: 400;color: #000 !important;cursor: pointer;text-overflow: ellipsis;width: calc(100%);white-space: nowrap;overflow: hidden;" id="dm_name">DIRECT MESSAGES</h3>
            </div>
        </div>
    </div>
    <div class="row msg_chat_scroll px-1 mt-1" style="margin-left: auto;padding-bottom: 15px;margin-right: 0px;display: flex;width: 100%;position: relative;" id="dm_convo_link_people"></div>
</div>

<!-- chat msg content -->
<div class="col" style="display: flex;flex-direction: column;border-right: 1px solid #ded9d9;border-left: 0px solid #ded9d9;">
    <div class="row">
        <div class="col" style="background: transparent;border-bottom: 0px solid #ded9d9;">
            <div class="row">
                <div class="col-12 px-2 mx-0" style="z-index: 1085;cursor: pointer;text-align: start;display: flex;padding: 4px;border-bottom: 1px solid #f1f1f1;background-color: #fff;">
                    <div class="col-12" style="display: flex;flex-direction: row;justify-content: flex-start;overflow: hidden;text-overflow: ellipsis;">
                        <input type="hidden" id="dm_recepient_id">
                        <input type="hidden" id="selected_user_avatar">
                        <i class="fa fa-at" aria-hidden="true" style="margin-top: 4px;padding-right: 3px;"></i>
                        <h3 class="text-muted mb-0" style="font-family: myFirstFont;font-weight: 400;color: #000 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;" id="dm_recepient_name"></h3>
                    </div>
                </div>

                <div style="overflow-y: hidden;height: 90vh;width: 100%;">
                   <div style="position: absolute;bottom: 0px;width: 100%;">
                        <div class="col-12 msg_chat_scroll_steady" style="margin-bottom: 3px;max-height: calc(84vh);display: block;flex-direction: column;width: -webkit-fill-available;padding-top: 18px;" id="dm_msg_contents"></div>

                        <div class="col-12">
                            <div style="border: 0px solid red;border-radius: 5px;display: flex;flex-direction: row;align-items: flex-start;background-color: #dee2ea;padding: 5px;">
                                <span class="upload-media-icon">
                                    <!-- file upload should "multiple" and has no limit on file extensions -->
                                    <input type='file' name="dm_upload_media" id="dm_upload_media" accept=".png, .jpg, .jpeg, .gif"/>
                                    <label for="dm_upload_media"><i class="fa fa-plus-circle mx-2" aria-hidden="true" style="font-size: 21px;margin-top: 8px;color: #505050;margin-left: 13px !important;"></i></label>
                                </span>
                                <div class="card-text note-content px-2 msg_chat_scroll pastable" style="border: 0px solid #afafaf;max-height: 300px;font-size: 14px !important;font-family: inherit;white-space: pre-wrap;padding: 9px;border-radius: 3px;background-color: #dee2ea;color: #303030;-webkit-user-modify: read-write-plaintext-only;" rows="1" contenteditable="true" id="send_dm_msg_content" aria-describedby="basic-addon1" data-text="Type a message, shift + enter to make a new line, enter to send"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        <!-- <div class="col-12">
            <div class="row">
                <div class="form-group" style="width: -webkit-fill-available;">
                    <div class="input-group" style="height: calc(17vh);">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="display: flex;flex-direction: column;border-radius: 0px;border: 0px !important;">
                                <span class="badge badge-pill badge-default upload-media-icon" style="color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;" title="upload media">
                                    <input type='file' name="dm_upload_media" id="dm_upload_media" accept=".png, .jpg, .jpeg, .gif"/>

                                    <label for="dm_upload_media"><i class="far fa-image" style="font-size: 16px;"></i></label>
                                </span>
                                <span class="badge badge-pill badge-default upload-media-icon" style="color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;" title="upload file">
                                    <input type='file' name="dm_upload_file" id="dm_upload_file" accept=".xls, .docs, .csv, .text, .zip, .rar, .xlxs, .pptx, .sql, .iso"/>
                                    <label for="dm_upload_file"><i class="fa fa-paperclip" style="font-size: 16px;"></i></label>
                                </span>
                            </span>
                        </div>
                        
                        <textarea class="form-control msg_chat_scroll" aria-label="With textarea" id="send_dm_msg_content" placeholder="Type a message, shift + enter to make a new line, enter to send" style="font-size: 14px;font-family: myFirstFont;white-space: pre-wrap;resize: none;color: #000;border: 0px !important;box-shadow: none !important;border-radius: 0px;" rows="1"></textarea>
                    </div>
                </div>
            </div>
        </div> -->


    </div>
    
</div>