<?php
    $project_code = clean($_REQUEST["q"]);
    $getProjData = $project->getDetail($project_code);
    $isProjPm = isProjectManager($project_code);
?>
 <style>
ul{
margin-bottom: 3px;
padding: 0px;
list-style: none;
}

.table-bordered td{
padding: 2px;
}

.table {
margin-bottom: 0px;
}

td.counters{
width: 20px;
text-align: center;
}

td.tdnames{
padding-left: 5px;
}

td.thnames{
padding: 10px;
text-align: center;
}

.table-bordered td {
border: 1px solid #bbbbbb;
}

.teams{
border: 1px solid #bbbbbb;
}
.droptarget {
    height: 600px;
}

pre{
font-size: 90%;
margin-bottom: 0rem !important;
}

::-webkit-scrollbar {
  width: 10px;
}
</style>

<input type="hidden" id="is_pm" value="<?=$isProjPm?>">
<input type="hidden" id="has_user_id" value="<?=$_SESSION['system']['userid_']?>">
<!-- BREADCRUMBS -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
        <div class="row align-items-center py-4">
            <!-- <div class="col-lg-12 col-7">
                <nav aria-label="breadcrumb" class="float-right d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Default</li>
                    </ol>
                </nav>
            </div> -->
        </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-9">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0 pb-0">
                  <div class="row">
                    <div class="col-12">
                        <h3 class="mb-3">Task Board</h3>
                        <?php if(isProjectManager($project_code) == 1){?>
                          <div class="row">
                            <div class="col-4 form-group mb-0">
                                <select id="myproj-team-selected" class="form-control" onchange="getTeamMembers()">
                                    <option value="">-- filter task by group --</option>
                                    <?php 
                                        $myGroup = $project->getTeamByProjectCode($project_code);
                                        if(count($myGroup) > 0){
                                            foreach($myGroup as $myGrplist){
                                              $tm_name = getTeamName($myGrplist['team_code']);
                                    ?>
                                        <option value="<?=$myGrplist['team_code']?>"><?=$tm_name?></option>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="col-4 form-group mb-0">
                                <select id="myproj-member-selected" class="form-control" onchange="getTaskByFilter()">
                                    
                                </select>
                            </div>
                          </div>
                        <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="card-body border-0">
                    
                    <div class="row" id="user_task_container">
                        
                    </div>

                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0">Project Details</h3>
                    </div>
                    <div class="card-body border-0">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <label style="display: flex;flex-direction: column;"><b>Name:</b> <?=clean($getProjData["projectName"])?></label>
                            </div>
                            <div class="col-12">
                                <label style="display: flex;flex-direction: column;"><b>Code:</b> <?=clean($getProjData["projectCode"])?></label>
                            </div>
                            <div class="col-12">
                                <label style="display: flex;flex-direction: column;"><b>Description:</b> <?=clean($getProjData["projectDescription"])?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="card">
                    <div class="card-body border-0">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <?php 
                                  if($getProjData["status"] == 0){
                                ?>
                                  <a href="#" class="btn btn-primary btn-sm" onclick="openModalNewTask()">New Task</a>
                                <?php } ?>
                                <?php if($isProjPm == 1){?>
                                  <a href="index.php?page=myproject_settings&q=<?=$project_code?>" class="btn btn-success btn-sm">Project Settings</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php include_once 'views/modals/add_task_modal.php';?>
<script>
  var isPM = $("#is_pm").val();
    $(document).ready(function() {
        displayUserTask();
    });

  function getSelectedProject() {
    var projCode = $("#project_select").val();
    window.location = 'index.php?page=dashboard&proj=' + projCode;
  }

  function openModalNewTask() {
      $("#modal-add-task").modal({
        show: true,
        backdrop: 'static',
        keyboard: false
      });
  }

  function changeTaskByTeam() {
    // alert("on dev");
    var temcode = $("#team_select").val();
    $.post("ajax/select_team_dropdown.php",{
      temcode: temcode
    },function(data,status){
    	//console.log($("#TeamcodeSess").val());
      location.reload();
    });
  }

  $('#team_select').on('change', function() {
    changeTaskByTeam();
  });


  function homeViewMembers(code){
    $('#projectTeamMember').modal('show');
    $('input[name="projectTeamMemberCode"]').val(code);

    getProjectTeamMemberData();
  }

  function addNewTask(){
    var task_date = $("#task_date").val();
    var task_status = $("#task_status").val();
    var task_description = $("#task_description").val();
    var project_code = "<?=$project_code?>";
    if(task_status == '' || task_description == ''){
        alertMe("Some of the fields is blank, please fill in the fields.","warning");
    }else{
        $.post("ajax/add_task.php",{
          due_date:task_date,
          taskDescription:task_description,
          priority_status:task_status,
          projectCode:project_code
        },function(data,status){
            if(data == 1){
              alertMe('Successfully Saved!', 'success');
              $("#task_description").val('');
            }else{
              alertMe('Error saving!', 'danger');
            }
            displayUserTask();
            //$("#modal-add-task").modal('hide');
        });
    }
  }

  function displayUserTask() {
    var project_code = "<?=$project_code?>";
    $.post("ajax/load_user_task.php",{
        project_code:project_code
    },function(data){
        $('#user_task_container').html(data);
        draggable_task();
    });
  }

  function deletetask(id) {
      var result = confirm("Are you sure you want to delete task?");
      if(result){
        $.post("ajax/delete_task.php",{
          id: id
        },function(data,status){
            if(data == 1){
              alertMe("Task deleted!", "success");
            }else{
              alertMe("Something went wrong!", "danger");
            }
            displayUserTask();
        });
      }
  }

  function draggable_task() {
    var hasUserID = $("#has_user_id").val();
    if(hasUserID != ""){
      $( function() {
        $( "#sortable1, #sortable2" ).sortable({
          connectWith: ".drops"
        }).disableSelection();
      } );

      $( "#sortable1, #sortable2, #sortable3" ).droppable({
          drop: function( event, ui ) {
            var draggableId = ui.draggable.attr("id");
            var parent_id = event.target.id;

            if(parent_id == "sortable1"){
              updateType(draggableId, 0);
            }else if(parent_id == "sortable2"){
              updateType(draggableId, 1);
            }else{
              updateType(draggableId, 2);
            }
          }
      });
    }else{
      alertMe('User session is empty! Please log in again.', 'danger');
    }
  }

  function updateType(id, type) {
    $.post("ajax/update_task_type.php",{
      id: id,
      type: type
    },function(data,status){
      displayUserTask();
      alertMe('Task updated', 'success');
    });
  }

  function inviteTeam(user_id) 
  {
    $.post("ajax/invite_team_to_project.php",{
      id: user_id
    },function(data,status){
        if(data == 1){
          location.reload();
        }else{
          alertMe('Something went wrong!', 'danger');
        }
    });
  }
  

  function getTeamMembers() {
    var teamSelected = $("#myproj-team-selected").val();
    $.post("ajax/get_team_member.php",{
      teamSelected: teamSelected
    },function(data){
        $("#myproj-member-selected").html(data);
    });
  }

  function getTaskByFilter() {
    var project_code = "<?=$project_code?>";
    var memberSelected;
    if(isPM == 1){
      memberSelected = $("#myproj-member-selected").val();
    }else{
      memberSelected = "";
    }
    $.post("ajax/load_user_task.php",{
        project_code: project_code,
        memberSelected: memberSelected
    },function(data){
        $('#user_task_container').html(data);
        draggable_task();
    });
  }

</script>
