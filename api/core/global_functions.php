<?php

function RAND_GEN($length = 4) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
     $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return strtoupper($randomString);
   }
   

function INSERT_QUERY($table_name, $form_data , $last_id = 'N'){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            return $lastID;
        }else{
            return 0;
        }
    }else{
        if($return_insert){
            return 1;
        }else{
            return 0;
        }
    }
}

function SELECT_QUERY($type , $table , $params = ''){
    $inject = ($params=='')?"":"WHERE $params";
    $select_query = mysql_query("SELECT $type FROM $table $inject")or die(mysql_error());
    $fetch = mysql_fetch_array($select_query);
    return $fetch;
}

function SELECT_LOOP_QUERY($type , $table , $params = ''){
    $inject = ($params=='')?"":"WHERE $params";
    $fetch = mysql_query("SELECT $type FROM $table $inject")or die(mysql_error());
    while ($row = mysql_fetch_array($fetch)) {
        $data[] = $row;
    }
    return $data;
}

function UPDATE_QUERY($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
        $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql);
    if($return_query){
        return 1;
    }else{
        return 0;
    }
}

function DELETE_QUERY($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;

    $return_delete = mysql_query($sql);

    if($return_delete){
        return 1;
    }else{
        return 0;
    }
}


function getUserName($userID)
{
    $getUserName = SELECT_QUERY("name", "tbl_users", "user_id = '$userID'");
    return $getUserName[0];
}

function isProjectManager($projectCode)
{
    $optin_id = $_SESSION["system"]["userid_"];
    $getPM = SELECT_QUERY("proj_pm", "tbl_project", "projectCode = '$projectCode'");
    if($optin_id == $getPM[0]){
        $result = 1;
    }else{
        $result = 0;
    }

    return $result;
}

function log_activity($msg, $module, $user_id, $task_code)
{
    $data = array(
        'log' => clean($msg), 
        'module' => clean($module), 
        'date' => date("Y-m-d H:i:s"),
        'user_id' => $user_id,
        'task_code' => $task_code
    );
    INSERT_QUERY("tbl_activity_logs", $data);
}

function updateTaskCode()
{
    $loop_task = SELECT_LOOP_QUERY("*", "tbl_task" , "");
    if(count($loop_task) > 0){
        foreach($loop_task as $task_list){
            $date_mins = date('is');
            $rand = strtoupper(RAND_GEN().$date_mins);
            $data = array(
                'task_code' => $rand
            );
            UPDATE_QUERY("tbl_task", $data, "task_id='$task_list[task_id]'");
        }
    }
}

function getProjectByTask($taskCode)
{
    $projcode = SELECT_QUERY("projectCode", "tbl_task", "task_code = '$taskCode'");
    $projName = SELECT_QUERY("projectName", "tbl_project", "projectCode = '$projcode[0]'");
    return $projName[0];
}

function getUserActivity()
{
    $user_id = $_SESSION["system"]["userid_"];
    $loop_activity = SELECT_LOOP_QUERY("*", "tbl_activity_logs" , "user_id = '$user_id' ORDER BY log_id DESC LIMIT 100");
    if(count($loop_activity) > 0){
        foreach($loop_activity as $activity_list){
            $data[] = array(
                'log' => clean($activity_list["log"]), 
                'module' => clean($activity_list["module"]), 
                'date' => $activity_list['date'],
                'user_id' => $activity_list[user_id],
                'task_code' => clean($activity_list["task_code"]),
                'project_name' => getProjectByTask($activity_list["task_code"])
            );
        }
        return $data;
    }
}

function UserNotes(){
    $user_id = $_SESSION["system"]["userid_"];
    $loop_notes = SELECT_LOOP_QUERY("*", "tbl_note" , "user_id = '$user_id' ORDER BY note_id DESC");
    if(count($loop_notes) > 0){
        foreach($loop_notes as $note_list){
            $data[] = array(
                'id' => clean($note_list["note_id"]), 
                'title' => clean($note_list["note_title"]), 
                'content' => $note_list['note_content'],
                'user_id' => $note_list[user_id]
            );
        }
        return $data;
    }
}

function SearchUserNotes($search_q){
    $user_id = $_SESSION["system"]["userid_"];
    $loop_notes = SELECT_LOOP_QUERY("*", "tbl_note" , "user_id = '$user_id' AND (note_content LIKE '%$search_q%' OR note_title LIKE '%$search_q%') ORDER BY note_id DESC");
    if(count($loop_notes) > 0){
        foreach($loop_notes as $note_list){
            $data[] = array(
                'id' => clean($note_list["note_id"]), 
                'title' => clean($note_list["note_title"]), 
                'content' => $note_list['note_content'],
                'user_id' => $note_list[user_id]
            );
        }
        return $data;
    }
}

function getProjectMember($code){
    $query = SELECT_LOOP_QUERY("*","tbl_project_member","projectCode = '$code' AND user_id != 0 GROUP BY user_id");
    foreach($query as $getRow) {
        $data[] = array(
            'user_id'   => $getRow[user_id],
            'memberName' => clean(getUserName($getRow[user_id])),
            'role'      => $getRow[role_id]
        );
    }

    return $data;
}

function getProjectGroupMember($code){
    $query = SELECT_LOOP_QUERY("*","tbl_project_member","projectCode = '$code' AND user_id = 0 GROUP BY teamCode");
    foreach($query as $getRow) {
        $getTeamData = SELECT_QUERY("*","tbl_team","teamCode = '$getRow[teamCode]'");
        $data[] = array(
            'team_id'  => $getTeamData[team_id],
            'TeamCode' => $getTeamData["teamCode"],
            'TeamName' => $getTeamData["teamName"]
        );
    }

    return $data;
}

function getChannelName()
{
    $ch_id = $_SESSION['chat']['channel'];
    $cnvo_id = $_SESSION['chat']['convo'];
    $res = SELECT_QUERY("name","tbl_convo_channel","channel_id = '$ch_id' AND convo_id = '$cnvo_id'");
    return clean($res[0]);
}

function getConvoName()
{
    $cnvo_id = $_SESSION['chat']['convo'];
    $res = SELECT_QUERY("convo_name","tbl_convo","convo_id = '$cnvo_id'");
    return clean($res[0]);
}

function getSSEMsg($id)
{
    $res = SELECT_QUERY("msg","tbl_convo_msg","chat_id = '$id'");
    return clean($res[0]);
}

function getMyGroups($user_id)
{
    $loop_group = SELECT_LOOP_QUERY("*","tbl_team","created_by = '$user_id'");
    if(count($loop_group) > 0){
        foreach($loop_group as $group_list){
            $data[] = array(
                'team_id' => $group_list[team_id],
                'team_code' => clean($group_list["teamCode"]),
                'team_name' => clean($group_list["teamName"]),
            );
        }
        return $data;
    }
}

function getTeamName($teamCode)
{
    $res = SELECT_QUERY("teamName","tbl_team","teamCode = '$teamCode'");
    return clean($res[0]);
}

function getChangeLog()
{
    $getLog = SELECT_QUERY("*","tbl_change_log_header","active = 1");
    $loopFeature = SELECT_LOOP_QUERY("*","tbl_change_log_detail","log_id = '$getLog[log_id]'");
    if(count($loopFeature) > 0){
        foreach($loopFeature as $ftList){
            $data[] = array(
                'log_id' => $getLog[log_id],
                'date' => date("M d, Y", strtotime($getLog[date_up])),
                'feature' => $ftList["feature"]
            );
        }

        return $data;
    }
}

function getUserAvatar($user_id)
{
    $res = SELECT_QUERY("slug","tbl_users","user_id = '$user_id'");
    $showPic = ($res[0] != "")?$res[0]:'user_default_avatar.png';
    return USER_AVATAR_BASEPATH.$showPic;
}

function totalRequestBook()
{
    $user_id = $_SESSION["system"]["userid_"];
    $res = SELECT_QUERY("count(request_id)","tbl_request_logs","status = 0 GROUP BY status");
    $total = ($user_id == 19)?($res[0] > 0)?$res[0]:"":"";
    return $total;
}

function total_announcement_counter()
{
    $user_id = $_SESSION["system"]["userid_"];
    $res = SELECT_QUERY("count(an_notif_id)","tbl_announcement_notif","receiver_id = '$user_id' GROUP BY receiver_id");
    $total = ($res[0] > 0)?$res[0]:"";
    return $total;
}

function getAllFirstLetter($text)
{
    $words = explode(" ", $text);
    $acronym = "";
    foreach ($words as $w) {
        $acronym .= $w[0];
    }
    return strtoupper($acronym);
}

function getConvoAvatar($convoid)
{
    $getConvoData = SELECT_QUERY("slug, convo_name","tbl_convo","convo_id = '$convoid'");
    $data = ($getConvoData[0] != "")?'<img src="'.CONVO_AVATAR_BASEPATH.$getConvoData[0].'" style="width: 45px;height: 45px;object-fit: cover;cursor: pointer;" class="rounded-circle">':'<div style="width: 50px;height: 50px;object-fit: cover;cursor: pointer;" class="avatar rounded-circle">'.getAllFirstLetter(clean($getConvoData[1])).'</div>';
    return $data;
}

function read_all_announcement_notif()
{
    $user_id = $_SESSION["system"]["userid_"];
    $res = DELETE_QUERY("tbl_announcement_notif","receiver_id = '$user_id'");
    echo $res;
}

function getGroupAvatar($teamCode)
{
    $getGroupData = SELECT_QUERY("slug, teamName","tbl_team","teamCode = '$teamCode'");
    $data = ($getGroupData[0] != "")?'<img src="'.MYGROUP_AVATAR_BASEPATH.$getGroupData[0].'" style="width: 100%;height: 100%;object-fit: cover;" class="rounded-circle">':getAllFirstLetter(clean($getGroupData[1]));
    return $data;
}

function DM_GET_RECEPIENT()
{
    $user_id = $_SESSION["system"]["userid_"];
    $loop_membr = SELECT_LOOP_QUERY("*,IF(sender_id = '$user_id',channel_id ,sender_id ) AS rcp","tbl_convo_msg","convo_id = -1 AND (channel_id = '$user_id' OR sender_id = '$user_id') GROUP BY rcp");
    if(count($loop_membr) > 0){
        foreach($loop_membr as $mlist){
            $data[] = array(
                'rcp_id' => $mlist[rcp]
            );
        }
        return $data;
    }
}

function DM_TOTAL_BADGE_COUNTER()
{
    $user_id = $_SESSION["system"]["userid_"];
    $getTotalDmMsgNotif = SELECT_QUERY("count(convo_id) as total","tbl_convo_notif","receiver_id = '$user_id' AND convo_id = -1 GROUP BY convo_id");

    $data = ($getTotalDmMsgNotif[0] > 0)?$getTotalDmMsgNotif[0]:0;
    return $data;
}

function DM_COUNT_USER_BADGE($sender_id)
{
    $user_id = $_SESSION["system"]["userid_"];
    $getDmMsgNotif = SELECT_QUERY("count(receiver_id) as total","tbl_convo_notif","receiver_id = '$user_id' AND sender_id = '$sender_id' AND convo_id = -1 GROUP BY receiver_id");

    $total = ($getDmMsgNotif[0] > 0)?$getDmMsgNotif[0]:"";
    return $total;
}

function extension_icon($extension)
{   
    $extUP = strtoupper($extension);
    $icons_array = array("XLS", "DOCX", "CSV", "TXT", "ZIP", "EXE", "XLSX", "PPT", "PPTX");
    if(in_array($extUP, $icons_array)){
        $icon = FILE_ATTACHMENT_BASEPATH.$extUP.'.png';
    }else{
        $icon = $icon = FILE_ATTACHMENT_BASEPATH.'FILE.png';;
    }
    return $icon;
}

function populate_change_log(){
    $loop_users = SELECT_LOOP_QUERY("*","tbl_users","user_id != 1");
    if(count($loop_users) > 0){
        $cl = SELECT_QUERY("*","tbl_change_log_header","active = 1");
        foreach($loop_users as $userList){
            $data = array(
                'recipient_id' => $userList[user_id],
                'change_log_id' => $cl[log_id],
                'date' => date("Y-m-d H:i:s")
            );

            INSERT_QUERY("tbl_change_log_notif", $data);
            echo "done : ".$userList[user_id];
        }
    }
}

function depopulate_change_log($user_id){
    $res = DELETE_QUERY("tbl_change_log_notif","recipient_id = '$user_id'");
    echo $res;
}




