<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            
            <li class="nav-item">
              <div class="nav-link active" title="Dashboard" onclick="window.location='index.php?page=home'">
                <i class="ni ni-world-2 text-primary"></i>
                <div style="position: absolute;bottom: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;padding-top: 6px;" id="total_an_badge"><?=total_announcement_counter()?></span></div>
              </div>
              <hr class="menu_divider">
            </li>
            
            <li class="nav-item">
              <div class="nav-link active" title="Request Book" onclick="openModalRequestBook()">
                <i class="fas fa-book" style="color: #606060;"></i>
                <div style="position: absolute;bottom: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;padding-top: 6px;" id="total_rb_badge"><?=totalRequestBook()?></span></div>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link active" href="#" title="Notes" onclick="openModalMyNotes()">
                <i class="far fa-sticky-note" style="color: #606060;"></i>
              </a>
            </li>

            <li class="nav-item">
              <div class="nav-link active" title="Chat" onclick="openModalChat()">
                <i class="far fa-comment-alt" style="color: #606060;"></i>
                <div style="position: absolute;bottom: 0px;right: 0px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;padding-top: 6px;" id="total_msg_badge"><?=$chat->all_counter()?></span></div>
              </div>
            </li>

            <!-- loop projects -->
            <div id="active_project_container">
            </div>
            <!-- end loop projects -->

            <li class="nav-item">
              <a class="nav-link active" href="#" title="Create New Project" onclick="openModalNewProject()" style="background-color: transparent;">
                <i class="fas fa-plus" style="color: #606060;"></i>
              </a>
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  </nav>